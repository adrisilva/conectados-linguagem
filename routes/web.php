<?php

Route::group(['domain' => '{school}.'.env('MAIN_DOMAIN')], function(){

    Route::get('first-access', 'Site\UserController@firstAccess')->name('first.access');
    Route::put('change-password', 'Site\UserController@changePassword')->name('change.password');
    Route::get('', 'Site\SiteController@index')->name('site.index');

    Route::post('login', 'Auth\LoginController@login')->name('site.login');
    Route::post('logout', 'Auth\LoginController@logout')->name('site.logout');

    Route::get('admin', 'Admin\Auth\LoginController@showFormLogin')->name('admin.formLogin');
    Route::post('admin', 'Admin\Auth\LoginController@login')->name('admin.login');

    Route::group(['namespace' => 'Site', 'middleware' => 'first.access'], function(){
        Route::get('home', 'HomeController@index')->name('site.home');
        Route::get('montar-equipe', 'TeamController@create')->name('team.create');

        Route::get('get-alunos/{id}', 'TeamController@getAlunos')->name('get.alunos');
        Route::post('team-store', 'TeamController@store')->name('team.store');

        Route::get('literatura', 'HomeController@showLiteratureBooks')->name('show.literature');
        Route::get('poemas', 'HomeController@showPoems')->name('show.poems');
        Route::get('musica', 'HomeController@showMusic')->name('show.music');
        Route::get('ingles', 'HomeController@showEnglish')->name('show.english');
        Route::get('espanhol', 'HomeController@showSpanish')->name('show.spanish');

        Route::get('equipe/perfil', 'TeamController@showPerfil')->name('team.perfil');
        Route::get('equipes', 'TeamController@index')->name('team.index');
        Route::get('equipe/{id}', 'TeamController@showTeam')->name('team.show');
        Route::get('equipe/{id}/delete', 'TeamController@delete')->name('team.delete');
        Route::get('olimpiada/questoes', 'TeamController@questions')->name('team.questions');

        Route::get('user/perfil', 'UserController@showPerfil')->name('user.perfil');
        Route::post('notes', 'TeamController@cadastroNotas')->name('team.notas');

        Route::put('user/change-password', 'UserController@changePassword')->name('user.change.password');
    });

    Route::group(['prefix' => 'admin', 'middleware' => 'first.access'], function(){

        Route::group(['namespace' => 'Admin'], function(){
            Route::post('logout', 'Auth\LoginController@logout')->name('admin.logout');

            Route::get('dashboard', 'HomeAdminController@index')->name('dashboard');

            //Rotas para gestão dos alunos
            Route::get('alunos', 'UserAdminController@indexAlunos')->name('alunos.index');
            Route::get('aluno/search', 'UserAdminController@searchAluno')->name('alunos.search');
            Route::get('cadastro/aluno', 'UserAdminController@createAluno')->name('alunos.create');
            Route::post('cadastro/aluno', 'UserAdminController@storeAluno')->name('alunos.store');
            Route::get('aluno/{id}/editar', 'UserAdminController@editAluno')->name('aluno.edit');
            Route::put('aluno/{id}/update', 'UserAdminController@updateAluno')->name('aluno.update');
            Route::delete('aluno/delete', 'UserAdminController@deleteAluno')->name('aluno.delete');

            //Rotas para gestão dos professores
            Route::get('professores', 'UserAdminController@indexProfessores')->name('professores.index');
            Route::get('professor/search', 'UserAdminController@searchProfessor')->name('professores.search');
            Route::get('cadastro/professor', 'UserAdminController@createProfessor')->name('professores.create');
            Route::post('cadastro/professor', 'UserAdminController@storeProfessor')->name('professores.store');
            Route::get('professor/{id}/edit', 'UserAdminController@editProfessor')->name('professores.edit');
            Route::put('professor/{id}/update', 'UserAdminController@updateProfessor')->name('professores.update');
            Route::delete('professor/delete', 'UserAdminController@deleteProfessor')->name('professores.delete');

            //Rotas para gestão de equipes
            Route::get('equipes', 'EquipeAdminController@index')->name('equipes.index');
            Route::get('equipes/search', 'EquipeAdminController@search')->name('equipes.search');
            Route::delete('equipe/delete', 'EquipeAdminController@delete')->name('equipes.delete');

            Route::get('config', 'ConfigAdminController@index')->name('config.index');
            Route::post('config/{id}/change-status', 'ConfigAdminController@changeStatus')->name('config.change.status');
        });
    });
});