<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AlunoCreateMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public function __construct(User $user)
    {
       $this->user = $user;
    }

    public function build()
    {
        return $this->from('contato@alimentodamente.com.br')
                    ->subject("Conectados pela Linguagens e Códigos")
                    ->view('admin.emails.create-aluno')
                    ->with([
                        'name'  => $this->user->name,
                        'email' => $this->user->email,
                        'password' => $this->user->password,
                    ]);;
    }
}
