<?php 
namespace App\Services;

use App\Models\SystemConfig;

class SystemConfigService
{
	private $systemConfig;

	public function __construct(SystemConfig $systemConfig)
	{
		$this->systemConfig = $systemConfig;
	}

	public function verifyIssuesReleased()
	{
		$config = $this->systemConfig->where('name', 'issues_released')->first();

		return $config->status;
	}

}