<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTeamFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|unique:teams,name',
            'user_one'      => 'required|unique:teams,user_one',
            'user_two'      => 'required|unique:teams,user_two',
            'user_three'    => 'required|unique:teams,user_three',
            'teacher'       => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required'         => 'Nome da equipe obrigatório',
            'name.unique'           => 'Esse nome de equipe já está em uso',

            'user_one.required'     => 'Primeiro integrante obrigatório',
            'user_one.unique'       => 'O primeiro integrante já está em outra equipe',

            'user_two.required'     => 'Segundo integrante obrigatório',
            'user_two.unique'       => 'O segundo integrante já está em outra equipe',

            'user_three.required'   => 'Terceiro integrante obrigatório',
            'user_three.unique'     => 'O terceiro integrante já está em outra equipe',

            'teacher.required'      => 'Professor obrigatório'
        ];
    }
}
