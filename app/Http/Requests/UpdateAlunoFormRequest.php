<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAlunoFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required',
            'email'         => 'required|unique:users,email,'.$this->id,
            'registration'  => 'required|min:7|max:7|unique:users,registration,'.$this->id,
            'school_type'   => 'required',
            'shift'         => 'required',
            'password'      => 'nullable|min:6',
            'birth'         => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required'         => 'Nome obrigatório',

            'email.required'        => 'E-mail obrigatório',
            'email.unique'          => 'Esse e-mail já está sendo usado',

            'registration.required' => 'Matrícula obrigatória',
            'registration.unique'   => 'Essa matrícula já está cadastrada',
            'registration.min'      => 'Matrícula Inválida, mínimo de 7 caracteres',
            'registration.max'      => 'Matrícula Inválida, máximo de 7 caracteres',

            'school_type.required'  => 'Tipo de escola obrigatório',

            'shift.required'        => 'Horário obrigatório',

            'password.min'          => 'Senha de no mínimo 7 caracteres',
            
            'birth.required'        => 'Data de nascimento obrigatória'
        ];
    }
}
