<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProfessorFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'          => 'required',
            'email'         => 'required|unique:users,email',
            'registration'  => 'required|min:7|max:7|unique:users,registration',
            'matter'        => 'required',
            'birth'         => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required'         => 'Nome obrigatório',

            'email.required'        => 'E-mail obrigatório',
            'email.unique'          => 'Esse e-mail já está sendo usado',

            'registration.required' => 'Matrícula obrigatória',
            'registration.unique'   => 'Essa matrícula já está cadastrada',
            'registration.min'      => 'Matrícula Inválida, mínimo de 7 caracteres',
            'registration.max'      => 'Matrícula Inválida, máximo de 7 caracteres',

            'matter.required'       => 'Matéria obrigatória',

            'birth.required'        => 'Data de nascimento obrigatória'
        ];
    }
}
