<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTeamFormRequest;
use App\Models\Team;
use App\Models\User;
use App\Models\Score;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($school)
    {
        $user = Auth::user();
        $teams = Team::where('teacher', $user->id)->get();

        return view('site.team.index', compact('school', 'teams'));
    }

    public function create($school)
    {
    	$team_name 		= null;
    	$user       	= Auth::user();
    	$alunos			= User::doesnthave('team')
    						->doesnthave('team_user_two')
    						->doesnthave('team_user_three')
    						->where('type', 'Aluno')
    						->where('series', $user->series)
    						->where('id', '!=', $user->id)
    						->orderBy('name', 'asc')
					    	->get();

    	$professores 	= User::where('type', 'Professor')->get();

    	return view('site.team.create', compact('school', 'team_name', 'alunos', 'professores'));
    }

    public function getAlunos($school, $id)
    {
    	$user  = Auth::user();
    	$alunos	= User::doesnthave('team')
					->doesnthave('team_user_two')
					->doesnthave('team_user_three')
					->where('type', 'Aluno')
					->where('series', $user->series)
					->where('id', '!=', $user->id)
					->where('id', '!=', $id)
					->orderBy('name', 'asc')
			    	->get();

    	return response()->json($alunos, 200);
    }

    public function store(CreateTeamFormRequest $request, $school)
    {
    	$data_form = $request->except('_token');
    	$data_form['user_one'] = Auth::user()->id;

    	Team::create($data_form);

    	session()->flash('success', 'Sua equipe foi cadastrada com sucesso! Comece a estudar a partir de agora!');

    	return redirect('home');
    }

    public function showPerfil($school)
    {
        $user = Auth::user();

        $team = Team::where('user_one', $user->id)
                    ->orWhere('user_two', $user->id)
                    ->orWhere('user_three', $user->id)
                    ->with(['student_one', 'student_two', 'student_three', 'teacher_responsible'])
                    ->first();

        $score = Score::select('points')->where('team_id', $team->id)->get();

        if(count($score) == 0){
            $score = 0;
        }else{
            $score = $score[0]['points'];
        }

        $team_name = $team->name;

        return view('site.team.profile', compact('team', 'school', 'team_name', 'score'));
    }

    public function showTeam($school, $id)
    {
        $team = Team::where('id', $id)
                    ->with(['student_one', 'student_two', 'student_three', 'teacher_responsible'])
                    ->first();

        $score = Score::select('points')->where('team_id', $team->id)->get();

        if(count($score) == 0){
            $score = 0;
        }else{
            $score = $score[0]['points'];
        }


        return view('site.team.team', compact('school', 'team', 'score'));
    }

    public function delete($school, $id)
    {
        $team = Team::destroy($id);

        return redirect()->back();
    }

    public function questions($school)
    {
        $team_name = self::getTeamName();
        $user_id   = Auth::user()->id;
        $team      = Team::orWhere('user_one', $user_id)
                        ->orWhere('user_two', $user_id)
                        ->orWhere('user_three', $user_id)
                        ->get();

        $score = Score::select('points')->where('team_id', $team[0]['id'])->first();    

        return view('site.team.questions', compact('school', 'team_name', 'score'));
    }

    public function cadastroNotas($school, Request $request, Score $score)
    {
        $team_name = self::getTeamName();
        $user_id   = Auth::user()->id;
        $team      = Team::orWhere('user_one', $user_id)
                        ->orWhere('user_two', $user_id)
                        ->orWhere('user_three', $user_id)
                        ->get();
        $scores = 0;

        if($request->q1 == "e")
        {
            $scores++;
        }
        if($request->q2 == "c")
        {
            $scores++;
        }
        if($request->q3 == "c")
        {
            $scores++;
        }
        if($request->q4 == "c")
        {
            $scores++;
        }
        if($request->q5 == "d")
        {
            $scores++;
        }
        if($request->q6 == "c")
        {
            $scores++;
        }
        if($request->q7 == "a")
        {
            $scores++;
        }
        if($request->q8 == "d")
        {
            $scores++;
        }
        if($request->q9 == "e")
        {
            $scores++;
        }
        if($request->q10 == "d")
        {
            $scores++;
        }

        $data = [ 'team_id' => $team[0]['id'], 'points' => $scores];
        $score->create($data);

        return redirect('home');
    }

    public function getTeamName()
    {
        $user_id   = Auth::user()->id;
        $team      = Team::orWhere('user_one', $user_id)
                        ->orWhere('user_two', $user_id)
                        ->orWhere('user_three', $user_id)
                        ->get();

        if(count($team) >= 1){
            $team_name = $team[0]['name'];
        }else{
            $team_name = null;
        }

        return $team_name;
    }
}
