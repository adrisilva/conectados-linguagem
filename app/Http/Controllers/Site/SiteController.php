<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SiteController extends Controller
{
    public function index($school)
    {
    	if(Auth::check()){
    		return redirect()->route('site.home', $school);
    	}
    	return view('site.index', compact('school'));
    }
}
