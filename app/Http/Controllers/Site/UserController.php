<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function showPerfil($school)
    {
        $team_name = self::getTeamName();
        $user = Auth::user();

        if($user->type != 'professor'){
            $team = Team::where('user_one', $user->id)
                    ->orWhere('user_two', $user->id)
                    ->orWhere('user_three', $user->id)
                    ->with(['student_one', 'student_two', 'student_three', 'teacher_responsible'])
                    ->first();
        }
        
        return view('site.perfil', compact('school', 'team_name', 'team', 'user'));
    }

    public function showPerfilTeacher($school)
    {
        $user = Auth::user();
        return view('site.perfil', compact('school', 'user'));
    }

    public function changePassword(ChangePasswordRequest $request, $school)
    {
       $dataForm = $request->except(['_token', '_method']);
       $user = (Auth::guard('admin')->check() ?  Auth::guard('admin')->user() : Auth::user());
       $dataForm['password'] = bcrypt($dataForm['password']);
       $user->password = $dataForm['password'];

       if($user->first_access){
           $user->first_access = 0;
       }

       $user->save();

        if(Auth::guard('admin')->check()){
            return redirect('/admin/dashboard')->withSuccess('Senha alterada com sucesso.');
        }else{
            return redirect('/home')->withSuccess('Sua Senha foi alterada com sucesso.');
        }
    }
    
    public function getTeamName()
    {
        $user_id   = Auth::user()->id;
        $team      = Team::orWhere('user_one', $user_id)
                        ->orWhere('user_two', $user_id)
                        ->orWhere('user_three', $user_id)
                        ->get();

        if(count($team) >= 1){
            $team_name = $team[0]['name'];
        }else{
            $team_name = null;
        }

        return $team_name;
    }

    public function firstAccess()
    {
        return view('site.auth.change');
    }

}
