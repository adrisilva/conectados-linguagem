<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($school)
    {
        $team_name = self::getTeamName();
        return view('site.home', compact('school', 'team_name'));
    }

    public function showLiteratureBooks($school)
    {
        $team_name = self::getTeamName();
        return view('site.livros.literatura', compact('school', 'team_name'));
    }

    public function showPoems($school)
    {
        $team_name = self::getTeamName();
        return view('site.livros.poema', compact('school', 'team_name'));
    }

    public function showMusic($school)
    {
        $team_name = self::getTeamName();
        return view('site.livros.musica', compact('school', 'team_name'));
    }

    public function showEnglish($school)
    {
        $team_name = self::getTeamName();
        return view('site.livros.ingles', compact('school', 'team_name'));
    }

    public function showSpanish($school)
    {
        $team_name = self::getTeamName();
        return view('site.livros.espanhol', compact('school', 'team_name'));
    }

    public function getTeamName()
    {
        $user_id   = Auth::user()->id;
        $team      = Team::orWhere('user_one', $user_id)
                        ->orWhere('user_two', $user_id)
                        ->orWhere('user_three', $user_id)
                        ->get();

        if(count($team) >= 1){
            $team_name = $team[0]['name'];
        }else{
            $team_name = null;
        }

        return $team_name;
    }
}
