<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Score;
use App\Models\Team;
use Illuminate\Http\Request;

class EquipeAdminController extends Controller
{
	public $per_page;

	public function __construct()
	{
		$this->middleware('auth:admin');
		$this->per_page = 15;
	}

    public function index($school)
    {
    	$times = Team::paginate($this->per_page);
    	$dataForm = null;
    	return view('admin.equipes.index', compact('school', 'dataForm', 'times'));
    }

    public function delete(Request $request, $school)
    {
    	$team = Team::destroy($request->get('id'));

		session()->flash('success', 'Dados apagados com sucesso');

		return redirect()->route('equipes.index', $school);
    }

    public function search(Request $request, $school)
    {
    	$dataForm = $request->all();
		$dataForm['s'] = $dataForm['s'] == null ? '' : $dataForm['s'];

		$times = Team::all();

		if(is_string($dataForm['s'])){
			$times = Team::where('name', 'like', '%'.$dataForm['s'].'%');
		}

		$times = $times->orderBy('name', 'asc')->paginate($this->per_page);

		return view('admin.equipes.index', compact('school', 'times', 'dataForm'));
    }

    public function showScore($school)
    {
    	$dataForm = null;
    	$pontos = Score::paginate($this->per_page);
    	return view('admin.equipes.score', compact('school', 'pontos', 'dataForm'));
    }

}
