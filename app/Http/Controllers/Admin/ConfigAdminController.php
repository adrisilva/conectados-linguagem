<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SystemConfig;
use Illuminate\Http\Request;

class ConfigAdminController extends Controller
{
	private $systemConfig;

	public function __construct(SystemConfig $systemConfig)
	{
		$this->systemConfig = $systemConfig;
	}

    public function index($school)
    {
    	$configs = SystemConfig::all();
    	return view('admin.configs.index', compact('school', 'configs'));	
    }

    public function changeStatus($school, $id)
    {
    	$config = $this->systemConfig->find($id);
    	$statusChange = ($config->status ? 0 : 1);
    	
    	$config->status = $statusChange;
    	$config->save();

    	return response()->json([], 200);
    }
}
