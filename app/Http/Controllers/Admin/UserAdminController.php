<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAlunoFormRequest;
use App\Http\Requests\CreateProfessorFormRequest;
use App\Http\Requests\UpdateAlunoFormRequest;
use App\Http\Requests\UpdateProfessorFormRequest;
use App\Mail\AlunoCreateMail;
use App\Mail\ProfessorCreateMail;
use App\Models\User;
use App\Util\Util;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class UserAdminController extends Controller
{
	public $per_page;

	public function __construct()
	{
		$this->middleware('auth:admin');
		$this->per_page = 15;
	}

	public function indexAlunos($school)
	{
		$dataForm = null;
		$alunos  = User::where('type', 'Aluno')->orderBy('name', 'asc')->paginate($this->per_page);
		$courses = User::$courses;
		return view('admin.alunos.index', compact('school', 'alunos', 'courses', 'dataForm'));
	}

	public function createAluno($school)
	{
		$courses = User::$courses;
		$series = User::$series;
		return view('admin.alunos.create', compact('school', 'courses', 'series'));
	}

	public function storeAluno(Request $request, $school)
	{
		$data = $request->except('_token');

		$password = Util::generatePassword();

		$data['password'] = bcrypt($password);

		if($data['school_type'] != 'eeep')
			$data['course'] = "-";

		$aluno = User::create($data);

		if($aluno){
			$aluno->password = $password;
			Mail::to($aluno)->send(new AlunoCreateMail($aluno));

		}

		session()->flash('success', 'Aluno Cadastrado com sucesso');

		return redirect()->route('alunos.index', $school);
	}

	public function editAluno($school, $id)
	{
		$aluno = User::find($id);
		$courses = User::$courses;
		$series = User::$series;

		return view('admin.alunos.edit', compact('aluno', 'school', 'courses', 'series'));
	}

	public function updateAluno(UpdateAlunoFormRequest $request, $school, $id)
	{
		$dataForm = $request->except('_token');
		$user = User::find($id);

		if($dataForm['password'] != null){
			$dataForm['password'] = bcrypt($dataForm['password']);
		}else{
			$dataForm['password'] = $user->password;
		}

		$user->update($dataForm);

		session()->flash('success', 'Dados atualizados com sucesso');

		return redirect()->route('alunos.index', $school);
	}

	public function deleteAluno(Request $request, $school)
	{
		$user = User::destroy($request->get('id'));

		session()->flash('success', 'Dados apagados com sucesso');

		return redirect()->route('alunos.index', $school);
	}

	public function searchAluno(Request $request, $school)
	{
		$dataForm = $request->all();
		$dataForm['s'] = $dataForm['s'] == null ? '' : $dataForm['s'];
		$dataForm['c'] = $dataForm['c'] == null ? '' : $dataForm['c'];

		$courses = User::$courses;

		$alunos = User::where('type', 'Aluno');

		if(is_numeric($dataForm['s'])){
			$alunos = User::where('type', 'Aluno')->where('registration', $dataForm['s']);
		}else if(is_string($dataForm['s'])){
			$alunos = User::where('type', 'Aluno')->where('name', 'like', '%'.$dataForm['s'].'%');
		}

		if($dataForm['c'] != null){
			$alunos = $alunos->where('type', 'Aluno')->where('course', $dataForm['c']);
		}

		$alunos = $alunos->orderBy('name', 'asc')->paginate($this->per_page);

		return view('admin.alunos.index', compact('school', 'alunos', 'courses', 'dataForm'));
	}

	public function indexProfessores($school)
	{
		$dataForm = null;
		$professores = User::where('type', 'Professor')->orderBy('name', 'asc')->paginate($this->per_page);
		return view('admin.professores.index', compact('school', 'professores', 'dataForm'));
	}

	public function createProfessor($school)
	{
		$matters = User::$matters;
		return view('admin.professores.create', compact('school', 'matters'));
	}

	public function storeProfessor(CreateProfessorFormRequest $request, $school)
	{
		$data = $request->except('_token');

		$password = Util::generatePassword();

		$data['password'] = bcrypt($password);
		$data['type'] = 'Professor';

		$professor = User::create($data);

		if($professor){
			$professor->password = $password;
			Mail::to($professor)->send(new ProfessorCreateMail($professor));
		}

		session()->flash('success', 'Professor Cadastrado com sucesso');

		return redirect()->route('professores.index', $school);
	}

	public function editProfessor($school, $id)
	{
		$professor = User::find($id);
		$matters = User::$matters;
		return view('admin.professores.edit', compact('professor', 'school', 'matters'));
	}

	public function updateProfessor(UpdateProfessorFormRequest $request, $school, $id)
	{
		$dataForm = $request->except('_token');
		$user = User::find($id);

		if($dataForm['password'] != null){
			$dataForm['password'] = bcrypt($dataForm['password']);
		}else{
			$dataForm['password'] = $user->password;
		}

		$user->update($dataForm);

		session()->flash('success', 'Dados atualizados com sucesso');

		return redirect()->route('professores.index', $school);
	}

	public function deleteProfessor(Request $request, $school)
	{
		$user = User::destroy($request->get('id'));

		session()->flash('success', 'Dados apagados com sucesso');

		return redirect()->route('professores.index', $school);
	}

	public function searchProfessor(Request $request, $school)
	{
		$dataForm = $request->all();
		$dataForm['s'] = $dataForm['s'] == null ? '' : $dataForm['s'];
		
		$professores = User::where('type', 'Professor');

		if(is_numeric($dataForm['s'])){
			$professores = User::where('type', 'Professor')->where('registration', $dataForm['s']);
		}else if(is_string($dataForm['s'])){
			$professores = User::where('type', 'Professor')->where('name', 'like', '%'.$dataForm['s'].'%');
		}

		$professores = $professores->orderBy('name', 'asc')->paginate($this->per_page);

		return view('admin.professores.index', compact('school', 'professores', 'courses', 'dataForm'));
	}

}
