<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Team;
use App\Models\User;
use Illuminate\Http\Request;

class HomeAdminController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth:admin');
    }

    public function index($school)
    {
    	$alunos = User::where('type', 'Aluno')->count();
    	$equipes = Team::all()->count();

    	return view('admin.home', compact('school', 'alunos', 'equipes'));
    }
}
