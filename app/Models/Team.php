<?php

namespace App\Models;

use App\Scopes\SchoolScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
    use SoftDeletes;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new SchoolScope);
    }

    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'user_one', 'user_two', 'user_three', 'teacher'];

    public function student_one()
    {
    	return $this->belongsTo(User::class, 'user_one');
    }

    public function student_two()
    {
    	return $this->belongsTo(User::class, 'user_two');
    }

    public function student_three()
    {
    	return $this->belongsTo(User::class, 'user_three');
    }

    public function teacher_responsible()
    {
    	return $this->belongsTo(User::class, 'teacher'); 
    }
}
