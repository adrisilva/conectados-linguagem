<?php

namespace App\Models;

use App\Scopes\SchoolScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Score extends Model
{
    use SoftDeletes;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new SchoolScope);
    }

    protected $dates = ['deleted_at'];

   	protected $fillable = ['team_id', 'points'];

   	public function team()
   	{
   		return $this->belongsTo(Team::class, 'team_id');
   	}
}
