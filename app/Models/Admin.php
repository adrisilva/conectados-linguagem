<?php

namespace App\Models;

use App\Scopes\SchoolScope;
use App\Scopes\SchoolScopeAdmin;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admin extends Authenticatable
{
	use Notifiable;
    use SoftDeletes;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new SchoolScopeAdmin);
    }

    protected $dates = ['deleted_at'];
    protected $fillables = ['name', 'email', 'password', 'admin'];
}
