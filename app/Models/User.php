<?php

namespace App\Models;

use App\Scopes\SchoolScope;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new SchoolScope);
    }

    protected $fillable = [
        'name', 'registration', 'birth', 'course', 'series', 'shift', 'type', 'matter', 'email', 'password', 'active'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['deleted_at'];

    public static $courses =  [
            'Enfermagem'        => 'Enfermagem', 
            'Finanças'          => 'Finanças',
            'Informática'       => 'Informática',
            'Produção de Moda'  => 'Produção de Moda'
        ];

    public static $series  = [
            '7' => '7º Ano',
            '8' => '8º Ano',
            '9' => '9º Ano',
            '1' => '1º Ano do Ensino Médio',
            '2' => '2º Ano do Ensino Médio',
            '3' => '3º Ano do Ensino Médio',
        ];

    public static $matters = [
            'Algebra'           =>  'Algebra',          
            'Artes'             =>  'Artes',            
            'Base Técnica'      =>  'Base Técnica', 
            'Biologia'          =>  'Biologia', 
            'Educação Física'   =>  'Educação Física',
            'Física'            =>  'Física',
            'Geografia'         =>  'Geografia',
            'Geometria'         =>  'Geometria',
            'Gramática'         =>  'Gramática',
            'História'          =>  'História',
            'Literatura'        =>  'Literatura',
            'Química'           =>  'Química',
        ];

    public function getFirstNameAluno()
    {
        $nameArray = explode(' ', $this->name);
        return $nameArray[0];
    }

    public function team()
    {
        return $this->hasOne(Team::class, 'user_one');
    }

    public function team_user_two()
    {
        return $this->hasOne(Team::class, 'user_two');
    }

    public function team_user_three()
    {
        return $this->hasOne(Team::class, 'user_three');
    }
}
