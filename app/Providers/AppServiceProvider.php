<?php

namespace App\Providers;

use App\Models\Score;
use App\Models\Team;
use App\Observers\ScoreObserver;
use App\Observers\TeamObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Team::observe(TeamObserver::class);
        Score::observe(ScoreObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
