<?php

namespace App\Observers;

use App\Models\School;
use App\Models\Score;

class ScoreObserver
{
    private $school;

    public function __construct()
    {
        $this->school = School::where('slug', request()->school)->first();
    }
    /**
     * Handle the score "created" event.
     *
     * @param  \App\Models\Score  $score
     * @return void
     */
    public function created(Score $score)
    {
        //
    }

    /**
     * Handle the score "updated" event.
     *
     * @param  \App\Models\Score  $score
     * @return void
     */
    public function updated(Score $score)
    {
        //
    }

    /**
     * Handle the score "deleted" event.
     *
     * @param  \App\Models\Score  $score
     * @return void
     */
    public function deleted(Score $score)
    {
        //
    }

    /**
     * Handle the score "restored" event.
     *
     * @param  \App\Models\Score  $score
     * @return void
     */
    public function restored(Score $score)
    {
        //
    }

    /**
     * Handle the score "force deleted" event.
     *
     * @param  \App\Models\Score  $score
     * @return void
     */
    public function forceDeleted(Score $score)
    {
        //
    }

    public function creating(Score $score)
    {
        $score->school_id = $this->school->id;
    }
}
