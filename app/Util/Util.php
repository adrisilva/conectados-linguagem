<?php 

namespace App\Util;

use Carbon\Carbon;

class Util
{

	public static function calculateAge($birth)
	{
		$data = explode('-', $birth);
		$dt = Carbon::now();
		$age  = $dt->year - $data[0];
		return $age;
	}

	public static function generatePassword()
	{
		$password = null;
		for($i = 1; $i <= 6; $i++){
			$num = rand(1, 9);
			$password .= $num;
		}
		return $password;
	}


}
