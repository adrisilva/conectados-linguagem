<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Conectados Pela Linguagens e Códigos</title>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="HexaFlip: A Flexible 3D Cube Plugin" />
    <meta name="keywords" content="hexaflip, 3d cube, css3 javascript, plugin, perspective, rotation, transition" />
    <meta name="author" content="Dan Motzenbecker for Codrops" />

    <link rel="stylesheet" type="text/css" href="{!! asset('css/default.css') !!}" />
    <link href="{!! asset('css/hexaflip.css') !!}" rel="stylesheet" type="text/css">
    <link href="{!! asset('css/demo.css') !!}" rel="stylesheet" type="text/css">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
    <!-- Font Awesome & Pixeden Icon Stroke icon font-->
    <link rel="stylesheet" href="{!! asset('css/font-awesome.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/pe-icon-7-stroke.css') !!}">
    <!-- Google fonts - Roboto Condensed & Roboto-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Condensed:700|Roboto:300,400">
    <!-- lightbox-->
	{{--     <link rel="stylesheet" href="{!! asset('css/lightbox.min.css') !!}"> --}}
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{!! asset('css/style.default.css') !!}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{!! asset('css/custom.css') !!}">
    <!-- Favicon-->
	<link rel="shortcut icon" href={{asset("favicon.png")}}>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

	<style>
		#panel h2{
			padding: 6px;
			font-size: 25px;
			text-align: center;
			background-color: #1A1193;
			color: white;
			border: solid 1px #666;
			border-radius: 3px;
			opacity: 0.6;
		}
		#panel ul{
			list-style:none;
			width: 100%;
			height: 160px;
			border-radius: 3px;
			text-align: center;
			background-color: #00D7DB;

		}
		#panel ul li { display: inline;}
		#panel ul li img{
			width: 150px;
			height: 150px;
		}
		#flip {
			padding: 6px;
			font-size: 25px;
			text-align: center;
			background-color: #1A1193;
			color: white;
			border: solid 1px #666;
			border-radius: 3px;
			opacity: 0.6;
		}

		#panel {
			padding: 40px;
			display: none;
			background-color: #f4511e;
			color: #fff;
			padding: 100px 25px;
		}

		.container-fluid {
			padding: 60px 50px;
		}
		.bg-grey {
			background-color: #f6f6f6;
		}
		.logo-small {
			color: #f4511e;
			font-size: 50px;
		}
		.logo {
			color: #f4511e;
			font-size: 200px;
		}
		.thumbnail {
			padding: 0 0 15px 0;
			border: none;
			border-radius: 0;
		}
		.thumbnail img {
			width: 100%;
			height: 100%;
			margin-bottom: 10px;
		}
		.carousel-control.right, .carousel-control.left {
			background-image: none;
			color: #f4511e;
		}
		.carousel-indicators li {
			border-color: #f4511e;
		}
		.carousel-indicators li.active {
			background-color: #f4511e;
		}
		.item h4 {
			font-size: 19px;
			line-height: 1.375em;
			font-weight: 400;
			font-style: italic;
			margin: 70px 0;
		}
		.item span {
			font-style: normal;
		}
		@media screen and (max-width: 768px) {
			.col-sm-4 {
				text-align: center;
				margin: 25px 0;
			}
		}
	</style>
</head>
<body>
	@inject('systemConfigs', 'App\Services\SystemConfigService')
	<header class="header">
      <div role="navigation" class="navbar navbar-default">
        <div class="container">
          <div class="navbar-header"><a href="{!! url('/') !!}" class="navbar-brand" style="color: #ef5285; font-size: 13pt;">Conectados Pelas Linguagens e Códigos</a>
            <div class="navbar-buttons">
              <button type="button" data-toggle="collapse" data-target=".navbar-collapse" class="navbar-toggle navbar-btn">Menu<i class="fa fa-align-justify"></i></button>
            </div>
          </div>
          <div id="navigation" class="collapse navbar-collapse navbar-right">
            <ul class="nav navbar-nav">
				<li><a href="http://{!! request()->school.'.'.env('MAIN_DOMAIN') !!}"><i class="fas fa-arrow-circle-left"></i>Voltar</a></li>
              {{--<li><a href="">Sobre nós</a></li>--}}
              <li><a href="https://alimentodamente.com.br/adm/site_adm/public/contato">Contato</a></li>
            
	            @if(Auth::check() && Auth::user()->type == "Aluno")
	            	
	            	<li class="dropdown" id="olimpiada"><a href="#" data-toggle="dropdown" class="dropdown-toggle" >Olímpiada <i class="fas fa-book"></i><b class="caret"></b></a>
		            	<ul class="dropdown-menu">
	            		   	@if($team_name != null)
	            		   		<li><a href="{!! url('equipe/perfil') !!}"><i class="fas fa-users"></i> {!! $team_name !!}</a></li>
				                
				                @if($systemConfigs->verifyIssuesReleased())
				                	<li><a href="{!! url('olimpiada/questoes') !!}"><i class="fas fa-question"></i> Questões </a></li>
				                @endif

	            		   	@else
	            		   		<li><a href="{{ url('montar-equipe') }}"><i class="fas fa-users"></i> Forme Sua Equipe</a></li>
	            		   	@endif
	            			<li><a href="{!! url('literatura') !!}"><i class="fas fa-book-open"></i> Livros</a></li>
		            	</ul>
		            </li>

		            <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle" id="usuario">{{Auth::user()->getFirstNameAluno()}} <i class="far fa-user-circle"></i><b class="caret"></b></a>
		            	<ul class="dropdown-menu">
		            		<li>
		            			<a href="{!! url('user/perfil') !!}" data-toggle="modal" data-target="">&nbsp;<i class="fas fa-user"></i> Seu Perfil</a>
		            		</li>
		            		<li>
		            			<a href="#" data-toggle="modal" data-target="#passwordModal">&nbsp;<i class="fas fa-key"></i> Mudar Senha</a>
		            		</li>
		            		<li>
		            			<a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
		            				<i class="fa fa-fw fa-power-off"></i>Sair
		            			</a>
		            			<form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
		            				@csrf
		            			</form>
		            		</li>   
		            	</ul>
		            </li>
	            @elseif(Auth::check() && Auth::user()->type == "Professor")
	            	<li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle" id="user_name">Olímpiada <i class="fas fa-book"></i><b class="caret"></b></a>
		            	<ul class="dropdown-menu">
	            		   	<li><a href="{!! url('equipes') !!}"><i class="fas fa-users"></i> Equipes</a></li>
	            			<li><a href="{!! url('literatura') !!}"><i class="fas fa-book-open"></i> Livros</a></li>
		            	</ul>
		            </li>

		            <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle" id="user_name">{{Auth::user()->name}} <i class="far fa-user-circle"></i><b class="caret"></b></a>
		            	<ul class="dropdown-menu">
		            		<li>
		            			<a href="{!! url('user/perfil') !!}" data-toggle="modal" data-target="">&nbsp;<i class="fas fa-user"></i> Seu Perfil</a>
		            		</li>
		            		<li>
		            			<a href="#" data-toggle="modal" data-target="#passwordModal">&nbsp;<i class="fas fa-key"></i> Mudar Senha</a>
		            		</li>
		            		<li>
		            			<a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
		            				<i class="fa fa-fw fa-power-off"></i>Sair
		            			</a>
		            			<form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
		            				@csrf
		            			</form>
		            		</li>   
		            	</ul>
		            </li>
	            @else
	            	<a href="#" data-toggle="modal" data-target="#login-modal" class="btn navbar-btn btn-ghost pull-left"><i class="fas fa-sign-in-alt"></i> Login</a>
	            @endauth
            </ul>
          </div>
        </div>
      </div>
    </header>
    <!-- *** LOGIN MODAL *** -->
    <div id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true" class="modal fade">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close">×</button>
            <h4 id="Login" class="modal-title">Entrar</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('login') }}" method="post">
                @csrf

                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input type="email" name="email" class="form-control" value="{{ old('email') }}"
                            placeholder="E-Mail">
                    {{-- <span class="glyphicon glyphicon-envelope form-control-feedback"></span> --}}
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <input type="password" name="password" class="form-control"
                            placeholder="Senha">
                    {{-- <span class="glyphicon glyphicon-floppy-disk form-control-feedback"></span> --}}
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <p class="text-center">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
                </p>
            </form>
            <p class="text-center text-muted">Não é registrado?</p>
            <p class="text-center text-muted">"Procure ser um homem de valor, em vez de ser um homem de sucesso."
              <strong>Albert Einstein</strong></p>
            </div>
          </div>
        </div>
  	</div>

  	<div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  		<div class="modal-dialog" role="document">
  			<div class="modal-content">
  				<div class="modal-header">
  					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  					<h4 class="modal-title" id="title">Alterar Senha</h4>
  				</div>
  				<div class="modal-body">
  					<form action="{!! url('user/change-password') !!}" method="POST">
  					@csrf
					@method('PUT')
  					<div class="row">
  						<div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
  							<label class="col-sm-4">Nova Senha</label>

  							<div class="col-sm-8">
  								<input type="password" class="form-control" name="password" id="password">
  							</div>
						</div>
  					</div>
  					<div class="row">
  						<div class="col-sm-4"></div>
  						<div class="col-sm-8 form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
  							@if ($errors->has('password'))
							<span class="help-block">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
							@endif
  						</div>
  					</div>
  					<p></p>
  					<div class="row">
  						<div class="form-group">
  							<label class="col-sm-4">Confirmar senha</label>

  							<div class="col-sm-8">
  								<input type="password" class="form-control" name="password_confirmation" id="password_confirmation">
  							</div>
  						</div>
  						@if ($errors->has('password_confirmation'))
  							<span class="help-block">
  								<strong>{{ $errors->first('password_confirmation') }}</strong>
  							</span>
						@endif
  					</div>

  				</div>
  				<div class="modal-footer">
  					<button class="btn btn-primary" data-dismiss="modal">Fechar</button>
  					<button class="btn btn-primary" type="submit">Salvar</button>
  					</form>
  				</div>
  			</div>
  		</div>
  	</div>

	<div class="container-fluid">
		@yield('content')
	</div>

	<footer class="footer">
      <div class="footer__copyright">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <p>&copy;2018 Conectados LC</p>
            </div>
            <div class="col-md-6">
              <p class="credit">A oportunidade é sua  <a href="#" class="external">e a evolução é nossa</a></p>     
            </div>
          </div>
        </div>
      </div>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>

    <script src="{!! asset('js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('js/jquery.cookie.js') !!}"> </script>
    {{-- <script src="{!! asset('js/lightbox.min.js') !!}"></script> --}}
    <script src="{!! asset('js/front.js') !!}"></script>
  	<script src="{!! asset('js/hexaflip.js') !!}"></script>
  

	@if($errors->has('password'))
        <script type="text/javascript">
            $('#passwordModal').modal('show');
        </script>
    @endif

    @if(!Auth::check())
    	@if($errors->has('password') || $errors->has('email'))
        	<script type="text/javascript">
            	$('#login-modal').modal('show');
        	</script>
    	@endif
    @endif

	<script> 
		$(document).ready(function(){ 
			$("#flip").click(function(){
				$("#panel").toggle(5000);
			});
		});
	</script>

	@yield('post-scripts')
</body>
</html>