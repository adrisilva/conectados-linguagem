<div class="jumbotron">
  <div class="container text-center">
    <h1>Livros e Mídias</h1>      
    <p>Alimente a mente e a deixe contente.</p>
  </div>
</div>

<nav class="navbar navbar-default">
  <div class="container-fluid">
   <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>                        
   </button>

 </div>
 <div class="collapse navbar-collapse" id="myNavbar">
  <ul class="nav navbar-nav">
   <li id="literatura"><a href="{!! url('literatura') !!}">Literatura</a></li>
   <li id="poemas"><a href="{!! url('poemas') !!}">Poemas</a></li>
   <li id="musica"><a href="{!! url('musica') !!}">Músicas</a></li>
   <li id="ingles"><a href="{!! url('ingles') !!}">Inglês</a></li>
   <li id="espanhol"><a href="{!! url('espanhol') !!}">Espanhol</a></li>
 </ul>
</div>
</div>
</nav>