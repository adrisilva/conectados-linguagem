    <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js"></script>
    <script>
        iziToast.settings({
            timeout: 2000,
            resetOnHover: true,
            transitionIn: 'flipInX',
            transitionOut: 'flipOutX',
        });
    </script>

    @if(session('success'))
    <script>
        iziToast.success({
            title: 'OK',
            message: "{{session('success')}}",
        });
    </script>
    @endif
