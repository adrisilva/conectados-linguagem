@extends('adminlte::page')

@section('content_header')
<h1>Configurações</h1>

{{-- <br>
<a class="btn btn-social btn-success" href="{!! route('alunos.create', $school) !!}">
	<i class="fa fa-plus"></i> Cadastrar Aluno
</a>
--}}
<ol class="breadcrumb">
	<li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
	<li><a href="">Configurações</a></li>
</ol>
@stop

@section('content')
<div class="box">
	
	<div class="box-body">
		<div class="table-responsive">
			<table class="table table-bordered">
				<thead>
					<tr role="row">
						<th>Nome</th>
						<th>Liberar</th>
					</tr>
				</thead>

				<tbody>

					@forelse ($configs as $config)
						<tr>
							<td>{{$config->label}}</td>
							<td>
								<input type="checkbox" 
									data-toggle="toggle" 
									data-on="Sim" data-off="Não" 
									data-style="ios" 
									class="switch"
									data-id={!! $config->id !!}
									{!! ($config->status ? 'checked' : '') !!}>
							</td>
						</tr>
					@empty
						<tr>
							<th colspan="2">Sem Configurações</th>
						</tr>
					@endforelse

				</tbody>
			</table>
		</div>

	</div>
	
	<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="crossorigin="anonymous"></script>
	<script>

		$(function() {
    		$('.switch').change(function() {
      			var id 	= $(this).data('id');
      			var url = '{!! url('admin/config/{id}/change-status') !!}'.replace('{id}', id);
      			$.ajax({
      				url: url,
      				method: 'POST',
      				data: {'_token': '{!! csrf_token() !!}'}
      			});
    		})
  		})
	</script>

@stop
