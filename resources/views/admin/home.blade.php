@extends('adminlte::page')

@section('content_header')
<h1>Painel de Controle</h1>
<ol class="breadcrumb">
	<li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="box-body">
		<div class="col-lg-3 col-xs-6">
			<div class="small-box bg-yellow">
				<div class="inner">
					<h3>{!! $alunos !!}</h3>
					<p>Alunos Cadastrados</p>
				</div>
				<div class="icon">
					<i class="ion ion-person-add"></i>
				</div>
				<a href={{url("admin/alunos")}} class="small-box-footer">Mais Informações <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>

		<div class="col-lg-3 col-xs-6">
			<div class="small-box bg-green">
				<div class="inner">
					<h3>{!! $equipes !!}</h3>
					<p>Equipes Cadastrados</p>
				</div>
				<div class="icon">
					<i class="ion ion-ios-people"></i>
				</div>
				<a href={{url("admin/equipes")}} class="small-box-footer">Mais Informações <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
	</div>
</div>
@stop