@extends('adminlte::page')

@section('content_header')
<h1>Equipes</h1>

<ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
    <li><a href="">Equipes</a></li>
</ol>
@stop

@section('content')
<div class="box">
	<div class="box-header with-border">

		{!! Form::model($dataForm, ['url' => ['admin/equipes/search'] ,'method' => 'get']) !!}

			{!! Form::label('s', 'Buscar', ['class' => 'col-sm-1']) !!}

			<div class="col-sm-9">
				{!! Form::text('s', null,['class' => 'form-control input-sm', 'placeholder' => 'Nome da equipe']) !!}
			</div>

			<div class="col-sm-2">
				<button class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
			</div>

		{!! Form::close() !!}
	</div>
	<div class="box-body">
		<div class="table-responsive">
			<table class="table table-bordered">
				<thead>
					<tr role="row">
						<th>Nome Equipe</th>
						<th>Primeiro Integrante</th>
						<th>Segundo Integrante</th>
						<th>Terceiro Integrante</th>
						<th>Professor Responsável</th>
						<th>Excluir</th>
					</tr>
				</thead>

				<tbody>

				@forelse ($times as $time)
				<tr>
					<td>{{$time->name}}</td>
					<td>{{$time->student_one->name}}</td>
					<td>{{$time->student_two->name}}</td>
					<td>{{$time->student_three->name}}</td>
					<td>{{$time->teacher_responsible->name}}</td>
					<td>
						<a href="#{{$time->id}}" class="btn btn-danger" data-confirm='Tem certeza que deseja excluir os dados desse aluno?'>
							<i class="fa fa-trash"></i>
						</a>
					</td>
				</tr>
				@empty
				<tr>
					<th colspan="7">Nenhuma Equipe Encontrada</th>
				</tr>
				@endforelse

				</tbody>
			</table>
		</div>

		@if(isset($dataForm))
			{!! $times->appends($dataForm)->links() !!}
		@else
			{!! $times->links() !!}
		@endif
	</div>

{!! Form::open(['url' => ['admin/equipe/delete'], 'method' => 'delete', 'id' => 'form_delete']) !!}
    {!! Form::hidden('id') !!}
{!! Form::close() !!}

@include('layouts.includes.notifications')
@include('layouts.includes.modal-delete')

@stop
