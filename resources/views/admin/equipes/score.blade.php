@extends('adminlte::page')

@section('content_header')
<h1>Pontuações</h1>

<ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
    <li><a href="">Equipes</a></li>
    <li><a href="">Pontuação</a></li>
</ol>
@stop

@section('content')
<div class="box">
	<div class="box-body">
		<div class="table-responsive">
			<table class="table table-bordered">
				<thead>
					<tr role="row">
						<th>Nome Equipe</th>
						<th>Pontuação Total</th>
					</tr>
				</thead>

				<tbody>

				@forelse ($pontos as $ponto)
				<tr>
					<td>{{$ponto->team->name}}</td>
					<td>{{$ponto->points}}</td>
				</tr>
				@empty
				<tr>
					<th colspan="7">Nenhuma Pontuação Encontrada</th>
				</tr>
				@endforelse

				</tbody>
			</table>
		</div>

		@if(isset($dataForm))
			{!! $pontos->appends($dataForm)->links() !!}
		@else
			{!! $pontos->links() !!}
		@endif
	</div>

@include('layouts.includes.notifications')
@include('layouts.includes.modal-delete')

@stop
