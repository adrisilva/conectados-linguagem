@extends('adminlte::page')

@section('content_header')
<h1>Alunos</h1>

{{--<br>--}}
{{--<a class="btn btn-social btn-success" href="{!! route('alunos.create', $school) !!}">--}}
	{{--<i class="fa fa-plus"></i> Cadastrar Aluno--}}
{{--</a>--}}

<ol class="breadcrumb">
	<li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
	<li><a href="">Alunos</a></li>
</ol>
@stop

@section('content')
<div class="box">
	<div class="box-header with-border">

		{!! Form::model($dataForm, ['url' => ['admin/aluno/search'] ,'method' => 'get']) !!}

		{!! Form::label('s', 'Buscar', ['class' => 'col-sm-1']) !!}

		<div class="col-sm-4">
			{!! Form::text('s', null,['class' => 'form-control input-sm', 'placeholder' => 'Matrícula ou Nome do Aluno']) !!}
		</div>

		{!! Form::label('c', 'Curso', ['class' => 'col-sm-1']) !!}

		<div class="col-sm-4">
			{!! Form::select('c', $courses, null, ['class' => 'form-control', 'placeholder' => 'Selecione o curso do aluno']); !!}
		</div>

		<div class="col-sm-2">
			<button class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
		</div>

		{!! Form::close() !!}
	</div>
	<div class="box-body">
		<div class="table-responsive">
			<table class="table table-bordered">
				<thead>
					<tr role="row">
						<th>Matrícula</th>
						<th>Nome</th>
						<th>E-Mail</th>                            
						<th>Curso</th>
						<th>Série</th>
						{{--<th>Editar</th>--}}
						{{--<th>Excluir</th>--}}
					</tr>
				</thead>

				<tbody>

					@forelse ($alunos as $aluno)
					<tr>
						<td>{{$aluno->registration}}</td>
						<td>{{$aluno->name}}</td>
						<td>{{$aluno->email}}</td>
						<td>{{$aluno->course}}</td>
						<td>{{$aluno->series}}</td>
						{{--<td><a href="{!! route('aluno.edit', [$school, $aluno->id]) !!}" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>--}}
						{{--<td>--}}
							{{--<a href="#{{$aluno->id}}" class="btn btn-danger" data-confirm='Tem certeza que deseja excluir os dados desse aluno?'>--}}
								{{--<i class="fa fa-trash"></i>--}}
							{{--</a>--}}
						{{--</td>--}}
					</tr>
					@empty
					<tr>
						<th colspan="7">Nenhum Aluno Encontrado</th>
					</tr>
					@endforelse

				</tbody>
			</table>
		</div>

		@if(isset($dataForm))
		{!! $alunos->appends($dataForm)->links() !!}
		@else
		{!! $alunos->links() !!}
		@endif
	</div>

	{!! Form::open(['route' => ['aluno.delete', $school], 'method' => 'delete', 'id' => 'form_delete']) !!}
	{!! Form::hidden('id') !!}
	{!! Form::close() !!}

	@include('layouts.includes.notifications')
	@include('layouts.includes.modal-delete')

@stop
