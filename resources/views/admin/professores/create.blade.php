@extends('adminlte::page')

@section('content_header')

<h1>Cadastro Professor</h1>

<ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
    <li><a href="">Professores</a></li>
    <li><a href="">Cadastro</a></li>
</ol>
@stop

@section('content')

{!! Form::open(['route' => ['professores.store', $school], 'method' => 'POST']) !!}

<div class="box box-primary">

    <div class="box-body">

        @include('layouts.includes.alerts')

        <div class="row">
            <div class="form-group">
                {!! Form::label('name', 'Nome', ['class' => 'col-sm-2']) !!}

                <div class="col-sm-10">
                    {!! Form::text('name', null,['class' => 'form-control', 'placeholder' => 'Digite o primeiro nome do professor']) !!}
                </div>
            </div>
        </div>

        <p></p>
        
        <div class="row">
            <div class="form-group">
                {!! Form::label('birth', 'Data de Nascimento', ['class' => 'col-sm-2']) !!}

                <div class="col-sm-4">
                    {!! Form::date('birth', null,['class' => 'form-control']) !!}
                </div>

                {!! Form::label('registration', 'Matrícula', ['class' => 'col-sm-2', 'max' => '7', 'min' => '7']) !!}

                <div class="col-sm-4">
                    {!! Form::text('registration', null,['class' => 'form-control', 'placeholder' => 'Digite a matrícula do professor']) !!}
                </div>
            </div>
        </div>

        <p></p>

        <div class="row">
            <div class="form-group">
                {!! Form::label('email', 'E-Mail', ['class' => 'col-sm-2']) !!}

                <div class="col-sm-10">
                    {!! Form::email('email', null,['class' => 'form-control', 'placeholder' => 'Digite o e-mail do professor']) !!}
                </div>
            </div>
        </div>

        <p></p>

        <div class="row">
            <div class="form-group">
                {!! Form::label('matter', 'Matéria', ['class' => 'col-sm-2']) !!}

                <div class="col-sm-10">
                    {!! Form::select('matter', $matters,null,['class' => 'form-control', 'placeholder' => 'Selecione a matéria que o professor leciona']) !!}
                </div>
            </div>
        </div>

        <p></p>

        <div class="row">
            <div class="form-group">
                <div class="col-sm-2">
                    <button type="button" class="btn btn-primary" onclick="history.go(-1)">Voltar</button>
                </div>
                <div class="col-sm-10">
                    {!! Form::submit('Salvar', ['class' => 'btn btn-block btn-success']) !!}
                </div>
            </div>
        </div>

    </div>
</div>

{!! Form::close() !!}
@stop