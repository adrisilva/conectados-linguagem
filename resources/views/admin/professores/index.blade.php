@extends('adminlte::page')

@section('content_header')
<h1>Professores</h1>

{{--<br>--}}
{{--<a class="btn btn-social btn-success" href="{!! route('professores.create', $school) !!}">--}}
	{{--<i class="fa fa-plus"></i> Cadastrar Professor--}}
{{--</a>--}}

<ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
    <li><a href="">Professores</a></li>
</ol>
@stop

@section('content')
<div class="box">
	<div class="box-header with-border">

		{!! Form::model($dataForm, ['url' => ['admin/professor/search'] ,'method' => 'get']) !!}

			{!! Form::label('s', 'Buscar', ['class' => 'col-sm-1']) !!}

			<div class="col-sm-9">
				{!! Form::text('s', null,['class' => 'form-control input-sm', 'placeholder' => 'Matrícula ou Nome do Professor']) !!}
			</div>

			<div class="col-sm-2">
				<button class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
			</div>

		{!! Form::close() !!}
	</div>
	<div class="box-body">
		<div class="table-responsive">
			<table class="table table-bordered">
				<thead>
					<tr role="row">
						<th>Matrícula</th>
						<th>Nome</th>
						<th>E-Mail</th>                            
						<th>Matéria</th>
						{{--<th>Editar</th>--}}
						{{--<th>Excluir</th>--}}
					</tr>
				</thead>

				<tbody>

					@forelse ($professores as $professor)
					<tr>
						<td>{{$professor->registration}}</td>
						<td>{{$professor->name}}</td>
						<td>{{$professor->email}}</td>
						<td>{{$professor->matter}}</td>
						{{--<td><a href="{!! route('professores.edit', [$school, $professor->id]) !!}" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>--}}
						{{--<td>--}}
							{{--<a href="#{{$professor->id}}" class="btn btn-danger" data-confirm='Tem certeza que deseja excluir os dados desse professor?'>--}}
								{{--<i class="fa fa-trash"></i>--}}
							{{--</a>--}}
						{{--</td>--}}
					</tr>
					@empty
					<tr>
						<th colspan="7">Nenhum Professor Encontrado</th>
					</tr>
					@endforelse

				</tbody>
			</table>
		</div>

		@if(isset($dataForm))
			{!! $professores->appends($dataForm)->links() !!}
		@else
			{!! $professores->links() !!}
		@endif
</div>

{!! Form::open(['route' => ['professores.delete', $school], 'method' => 'delete', 'id' => 'form_delete']) !!}
    {!! Form::hidden('id') !!}
{!! Form::close() !!}

@include('layouts.includes.notifications')
@include('layouts.includes.modal-delete')

@stop

