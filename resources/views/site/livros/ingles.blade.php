@extends('layouts.app')

@section('content')

@include('layouts.nav_livros')

<div class="container">    
	<div class="row">
		<div class="col-sm-4">
			<div class="panel panel-primary">
				<div class="panel-heading">BIRTHDAY’S ORIGIN</div>
				<div class="panel-body">
					In ancient Rome, there was the habit of celebrating the birthday of a person. There weren’t parties like we know today, but cakes were prepared and offers were made. Then, the habits of wishing happy birthday, giving gifts and lighting candles became popular as a way to protect the birthday person from devils and ensure good things to the next year in the person’s life. The celebrations only became popular like we know today after fourteen centuries, in a collective festival performed in Germany.
				</div>

			</div>
		</div>
		<div class="col-sm-4"> 
			<div class="panel panel-primary">
				<div class="panel-heading">DEPARTMENT STORE</div>
				<div class="panel-body">
					The first department store was created in Paris, in 1850. It was conceived as a big place with a lot of options in products and brands. They wouldn’t sell just a specific product: there you could  find clothes, shoes, jewels, make up and many others things. That concept remained until today. So, if you want to buy many things with good prices, you can choose from one of the thousands in the world.

					Every year, on the day after Thanksgiving, most of this stores have a big sale called Black Friday. On this day, it is possible to buy products with big discounts and you also have the option to buy things online. In 2016, more than 154 million people spent USD 3,3 billion dollars shopping only on the internet.
				</div>
				
			</div>
		</div>
		<div class="col-sm-4"> 
			<div class="panel panel-primary">
				<div class="panel-heading">WALK OF FAME</div>
				<div class="panel-body">
					Even if you have never visited Los Angeles, you have probably heard about the most famous sidewalk in the world: the Hollywood Walk of Fame. Every year, around 10 million tourists visit this street to take pictures with their idol’s star.

					Since 1960, the Hollywood Boulevard Avenue attracts visitors to see star-plaques with the names of celebrities: actors, singers, movie directors, stage performers or anyone who has contributed to make Hollywood known worldwide. Each pink star is made of a marble called terrazzo, a bronze shield with the honoree’s name and an icon of the celebrity job, like a camera, television or a microphone, for example.
				</div>
				
			</div>
		</div>
	</div>
</div><br>

<div class="container">    
	<div class="row">
		<div class="col-sm-4">
			<div class="panel panel-primary">
				<div class="panel-heading">TRY TO SLEEP</div>
				<div class="panel-body">
					Norma went to bed. It was eleven o'clock. She turned out the light. She lay in bed. It was dark. It was quiet. She couldn't sleep. She closed her eyes. She tried to sleep, but she couldn't. She turned the light back on. She opened her book. She started to read her book. It was a good book. She read one page. Then she read another page. After a while, she felt sleepy. She closed the book. She turned out the light. She closed her eyes. She went straight to sleep.
				</div>
				
			</div>
		</div>
		<div class="col-sm-4"> 
			<div class="panel panel-primary">
				<div class="panel-heading">TRUE STORIES</div>
				<div class="panel-body">A great way for learning a language! Read true stories and facts about life, nature and the universe, in bilingual, English and Portuguese, side by side texts. Practice your English or Portuguese!</div>
				
			</div>
		</div>
		<div class="col-sm-4"> 
			<div class="panel panel-primary">
				<div class="panel-heading">GRAB YOUR UMBRELLAS</div>
				<div class="panel-body">
					It looked like rain. The sky was gray. It was almost noon, but the sun was hidden by a gray blanket. It was cool. There were no birds flying anywhere. A couple of birds sat on the telephone wire. Bob was standing outside talking to Bill. They both had their hands in their pockets. They knew that it was probably going to rain shortly. A sudden breeze blew some leaves off a tree onto the sidewalk.

					A young woman wearing a dark blue coat and jeans walked by. She was walking a small dog. It was pure white, and pretty. It sniffed at a tree trunk. The woman waited patiently. Finally, the dog lifted its leg.

					Bob said that he liked the rain. It was a nice change from the usual hot Los Angeles weather. And the plants could always use the extra water. Bill said the only thing he didn’t like about rain was that all the motor oil on the streets would get washed into the ocean, and so would all the trash.

					“But that never stops the surfers,” Bob said. “They don’t seem to care what’s in the water, as long as there are waves to surf on.”
				</div>
				
			</div>
		</div>
	</div>
</div>

<script>
	document.getElementById('ingles').className = 'active';
</script>
@endsection