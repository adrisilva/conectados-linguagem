@extends('layouts.app')

@section('content')

@include('layouts.nav_livros')

<div class="container">    
	<div class="row">
		<div class="col-sm-4">
			<div class="panel panel-primary">
				<div class="panel-heading">PERFEIÇÃO</div>
				<div class="panel-body">

					<audio controls>
						<source src="{!! asset('audio/perfeicao.mp3') !!}" type="audio/mpeg">
						</audio> <br>
						<p align="center"><a href="https://www.vagalume.com.br/legiao-urbana/perfeicao-com-trecho-da-musica-ao-vivo.html">Clique aqui para ver a letra</a></p>
					</div>
					<div class="panel-footer">Artista: Legião Urbana <br> Álbum: O Descobrimento do Brasil <br> Data de lançamento: 1993</div>
				</div>
			</div>
			<div class="col-sm-4"> 
				<div class="panel panel-primary">
					<div class="panel-heading">CENSURA</div>
					<div class="panel-body">
						<audio controls>
							<source src="{!! asset('audio/censura.mp3') !!}" type="audio/mpeg">
							</audio> <br>
							<p align="center"><a href="https://www.vagalume.com.br/plebe-rude/censura.html">Clique aqui para ver a letra</a></p>
						</div>
						<div class="panel-footer">Artista: Plebe Rude <br>Álbum: Nunca Fomos tão Brasileiros <br>
						Data de lançamento: 1987</div>
					</div>
				</div>
				<div class="col-sm-4"> 
					<div class="panel panel-primary">
						<div class="panel-heading">BRASIS</div>
						<div class="panel-body">
							<audio controls>
								<source src="{!! asset('audio/brasis.mp3') !!}" type="audio/mpeg">
								</audio> <br>
								<p align="center"><a href="https://www.vagalume.com.br/seu-jorge/brasis.html">Clique aqui para ver a letra</a></p>
							</div>
							<div class="panel-footer">Artista: Seu Jorge <br>Álbum: Solo “Brasis”  <br>
							Data de lançamento: 2005</div>
						</div>
					</div>
				</div>
			</div><br>

			<div class="container">    
				<div class="row">
					<div class="col-sm-4">
						<div class="panel panel-primary">
							<div class="panel-heading">ALEGRIA ALEGRIA</div>
							<div class="panel-body">
								<audio controls>
									<source src="{!! asset('audio/alegria.mp3') !!}" type="audio/mpeg">
									</audio> <br>
									<p align="center"><a href="https://www.vagalume.com.br/caetano-veloso/alegria-alegria.html">Clique aqui para ver a letra</a></p>
								</div>
								<div class="panel-footer">Artista: Caetano Veloso <br>
									Álbum: Caetano Veloso <br>
								Data de lançamento: 1968</div>
							</div>
						</div>
						<div class="col-sm-4"> 
							<div class="panel panel-primary">
								<div class="panel-heading">A BANDA</div>
								<div class="panel-body">
									<audio controls>
										<source src="{!! asset('audio/banda.mp3') !!}" type="audio/mpeg">
										</audio> <br>
										<p align="center"><a href="https://www.vagalume.com.br/chico-buarque/a-banda.html">Clique aqui para ver a letra</a></p>
									</div>
									<div class="panel-footer">Artista: Chico Buarque <br>
										Álbum: Chico Buarque de Hollanda <br>
									Data de lançamento: 1966</div>
								</div>
							</div>
							<div class="col-sm-4"> 
								<div class="panel panel-primary">
									<div class="panel-heading">CÁLICE</div>
									<div class="panel-body">
										<audio controls>
											<source src="{!! asset('audio/calice.mp3') !!}" type="audio/mpeg">
											</audio> <br>
											<p align="center"><a href="https://www.vagalume.com.br/chico-buarque/calice.html">Clique aqui para ver a letra</a></p>
										</div>
										<div class="panel-footer">Artista: Chico Buarque <br>

											Álbum: Chico Buarque <br>
										Data de lançamento: 1978</div>
									</div>
								</div>
							</div>
						</div>

						<script>
							document.getElementById('musica').className = 'active';
						</script>
						@endsection
