@extends('layouts.app')

@section('content')

@include('layouts.nav_livros')

<div class="container">    
	<div class="row">
		<div class="col-sm-4">
			<div class="panel panel-primary">
				<div class="panel-heading">ETERNA MÁGOA</div>
				<div class="panel-body">
					O homem por sobre quem caiu a praga <br>
					Da tristeza do Mundo, o homem que é triste <br>
					Para todos os séculos existe <br>
					E nunca mais o seu pesar se apaga! <br> <br>
					Não crê em nada, pois, nada há que traga <br> 
					Consolo à Mágoa, a que só ele assiste. <br>
					Quer resistir, e quanto mais resiste <br>
					Mais se lhe aumenta e se lhe afunda a chaga. <br> <br> 
					Sabe que sofre, mas o que não sabe <br>
					É que essa mágoa infinda assim, não cabe <br>
					Na sua vida, é que essa mágoa infinda <br><br>

					
					Transpõe a vida do seu corpo inerme; <br>
					E quando esse homem se transforma em verme <br>
					É essa mágoa que o acompanha ainda!
				</div>
				<div class="panel-footer">Autor: Augusto dos Anjos <br> Período: Pré-Modernismo</div>
			</div>
		</div>
		<div class="col-sm-4"> 
			<div class="panel panel-primary">
				<div class="panel-heading">PSICOLOGIA DE UM VENCIDO</div>
				<div class="panel-body">
					Eu, filho do carbono e do amoníaco, <br> 
					Monstro de escuridão e rutilância, <br> 
					Sofro, desde a epigênese da infância, <br> 
					A influência má dos signos do zodíaco. <br> <br> 
					Produndissimamente hipocondríaco, <br> 
					Este ambiente me causa repugnância... <br> 
					Sobe-me à boca uma ânsia análoga à ânsia <br> 
					Que se escapa da boca de um cardíaco.  <br> <br> 
					Já o verme -- este operário das ruínas -- <br> 
					Que o sangue podre das carnificinas <br> 
					Come, e à vida em geral declara guerra, <br> <br>  
					Anda a espreitar meus olhos para roê-los, <br> 
					E há de deixar-me apenas os cabelos, <br> 
					Na frialdade inorgânica da terra!
				</div>
				<div class="panel-footer">Autor: Augusto dos Anjos <br> Período: Pré-Modernismo</div>
			</div>
		</div>
		<div class="col-sm-4"> 
			<div class="panel panel-primary">
				<div class="panel-heading">VERSOS ÍNTIMOS</div>
				<div class="panel-body">
					Vês! Ninguém assistiu ao formidável <br> 
					Enterro de sua última quimera. <br> 
					Somente a Ingratidão – esta pantera – <br> 
					Foi tua companheira inseparável! <br> <br> 
					Acostuma-te à lama que te espera! <br> 
					O homem, que, nesta terra miserável, <br> 
					Mora, entre feras, sente inevitável <br> 
					Necessidade de também ser fera.  <br> <br> 
					Toma um fósforo. Acende teu cigarro! <br> 
					O beijo, amigo, é a véspera do escarro, <br> 
					A mão que afaga é a mesma que apedreja. <br> <br>  
					Se alguém causa inda pena a tua chaga,<br>  
					Apedreja essa mão vil que te afaga, <br> 
					Escarra nessa boca que te beija!
				</div>
				<div class="panel-footer">Autor: Augusto dos Anjos <br> Período: Pré-Modernismo</div>
			</div>
		</div>
	</div>
</div><br>

<div class="container">    
	<div class="row">
		<div class="col-sm-4">
			<div class="panel panel-primary">
				<div class="panel-heading">AUTOPSICOGRAFIA</div>
				<div class="panel-body">
					O poeta é um fingidor. <br>
					Finge tão completamente<br>
					Que chega a fingir que é dor<br>
					A dor que deveras sente.<br><br>

					E os que leem o que escreve,<br>
					Na dor lida sentem bem,<br>
					Não as duas que ele teve,<br>
					Mas só a que eles não têm.<br><br>

					E assim nas calhas de roda<br>
					Gira, a entreter a razão,<br>
					Esse comboio de corda<br>
					Que se chama coração.
				</div>
				<div class="panel-footer">Autor: Fernando Pessoa <br> Período: Pré-Modernismo</div>
			</div>
		</div>
		<div class="col-sm-4"> 
			<div class="panel panel-primary">
				<div class="panel-heading">PRESSÁGIO</div>
				<div class="panel-body">
					O amor, quando se revela,<br>
					Não se sabe revelar.<br>
					Sabe bem olhar pra ela,<br>
					Mas não lhe sabe falar.<br><br>

					Quem quer dizer o que sente<br>
					Não sabe o que há de dizer.<br>
					Fala: parece que mente…<br>
					Cala: parece esquecer…<br><br>

					Ah, mas se ela adivinhasse,<br>
					Se pudesse ouvir o olhar,<br>
					E se um olhar lhe bastasse<br>
					Pra saber que a estão a amar!<br><br>

					Mas quem sente muito, cala;<br>
					Quem quer dizer quanto sente<br>
					Fica sem alma nem fala,<br>
					Fica só, inteiramente!<br><br>

					Mas se isto puder contar-lhe<br>
					O que não lhe ouso contar,<br>
					Já não terei que falar-lhe<br>
					Porque lhe estou a falar…
				</div>
				<div class="panel-footer">Autor: Fernando Pessoa <br> Período: Pré-Modernismo</div>
			</div>
		</div>
		<div class="col-sm-4"> 
			<div class="panel panel-primary">
				<div class="panel-heading">AMAR E SER AMADO</div>
				<div class="panel-body">
					Amar e ser amado! Com que anelo <br>
					Com quanto ardor este adorado sonho<br>
					Acalentei em meu delírio ardente<br>
					Por essas doces noites de desvelo!<br>
					Ser amado por ti, o teu alento<br>
					A bafejar-me a abrasadora frente!<br>
					Em teus olhos mirar meu pensamento,<br>
					Sentir em mim tu’alma, ter só vida<br><br>
					P’ra tão puro e celeste sentimento<br>
					Ver nossas vidas quais dois mansos rios,<br>
					Juntos, juntos perderem-se no oceano,<br>
					Beijar teus labios em delírio insano<br>
					Nossas almas unidas, nosso alento,<br>
					Confundido também, amante, amado<br>
					Como um anjo feliz... que pensamento!?


				</div>
				<div class="panel-footer">Autor: Castro Alves <br> Período: Romantismo 3° Geração</div>
			</div>
		</div>
	</div>
</div><br><br>


<script>
  document.getElementById('poemas').className = 'active';
</script>
@endsection