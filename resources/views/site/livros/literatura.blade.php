@extends('layouts.app')

@section('content')

@include('layouts.nav_livros')

<div class="container">    
  <div class="row">
    <div class="col-sm-4">
      <div class="panel panel-primary">
        <div class="panel-heading">TRISTE FIM DE POLICARPO QUARESMA</div>
        <div class="panel-body"><img src="{!! asset('images/triste.jpg') !!}" class="img-responsive" style="width:100%" alt="Image"><br>
          <p align="center"> <a href="http://www.ebooksbrasil.org/adobeebook/policarpoE.pdf">Livro em PDF</a></p></div>
        <div class="panel-footer">Autor: Lima barreto <br> Gênero: Romance <br> Período: Pré-Modernismo</div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-primary">
        <div class="panel-heading">OS LUSÍADAS</div>
        <div class="panel-body"><img src="{!! asset('images/lulu.jpg') !!}" class="img-responsive" style="width:100%" alt="Image"><br>
          <p align="center"> <a href="http://www.dominiopublico.gov.br/download/texto/ua000178.pdf">Livro em PDF</a></p></div>
        <div class="panel-footer">Autor: Luís de Camões <br> Gênero: Narrativo <br> Período: Romantismo</div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-primary">
        <div class="panel-heading">DOM CASMURRO</div>
        <div class="panel-body"><img src="{!! asset('images/dom.jpg') !!}" class="img-responsive" style="width:100%" alt="Image"><br>
          <p align="center"> <a href="http://www.dominiopublico.gov.br/download/texto/ua000194.pdf">Livro em PDF</a></p></div>
        <div class="panel-footer">Autor: Machado de Assis <br> Gênero: Romance <br> Período: Romantismo</div>
      </div>
    </div>
  </div>
</div><br>

<div class="container">    
  <div class="row">
    <div class="col-sm-4">
      <div class="panel panel-primary">
        <div class="panel-heading">O CORTIÇO</div>
        <div class="panel-body"><img src="{!! asset('images/cor.jpg') !!}" class="img-responsive" style="width:100%" alt="Image"><br>
          <p align="center"> <a href="http://objdigital.bn.br/Acervo_Digital/Livros_eletronicos/cortico.pdf">Livro em PDF</a></p></div>
        <div class="panel-footer">Autor: Aluísio Azevedo <br> Gênero: Romance Naturalista <br> Período: Romantismo</div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-primary">
        <div class="panel-heading">OS SERTÕES</div>
        <div class="panel-body"><img src="{!! asset('images/ser.jpg') !!}" class="img-responsive" style="width:100%" alt="Image"><br>
          <p align="center"> <a href="http://www.dominiopublico.gov.br/download/texto/bv000091.pdf">Livro em PDF</a></p></div>
        <div class="panel-footer">Autor: Euclides da Cunha <br> Gênero: Romance <br> Período: Pré-Modernismo</div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-primary">
        <div class="panel-heading">A MORENINHA</div>
        <div class="panel-body"><img src="{!! asset('images/mor.jpg') !!}" class="img-responsive" style="width:100%" alt="Image"><br>
          <p align="center"> <a href="http://objdigital.bn.br/Acervo_Digital/Livros_eletronicos/a_moreninha.pdf">Livro em PDF</a></p></div>
        <div class="panel-footer">Autor: Joaquim Manuel de Macedo <br> Gênero: Romance <br> Período: Romantismo</div>
      </div>
    </div>
  </div>
</div><br><br>

<script>
  document.getElementById('literatura').className = 'active';
</script>
@endsection
