@extends('layouts.app')

@section('content')

@include('layouts.nav_livros')

<div class="container">    
	<div class="row">
		<div class="col-sm-4">
			<div class="panel panel-primary">
				<div class="panel-heading">- MÁS ALLÁ DEL ESPEJO</div>
				<div class="panel-body">

					Frente a la concepción del cine como representación de la realidad existe otra, si bien
					menos conocida, según la cual la sabiduría del cine reside sobre todo en su capacidad de
					encarnar lo imaginario. Lo que aparece en la pantalla no es el mundo, en su evidencia o su
					concreción, sino un universo nuevo donde se mezclan objetos comunes y situaciones
					anómalas, hechos concretos y sensaciones impalpables, presencias reconocibles y entidades
					irreales, comportamientos habituales y lógicas sorprendentes. En resumen, 1
					el cine abre un
					espacio distinto, habitado por muchas más cosas de las que rodean nuestra vida.
					Deberemos hacer, por tanto, como Alicia cuando vuelve al País de las Maravillas; si
					queremos realizar de verdad el viaje al que hemos sido invitados, no bastará con asomarnos
					a la ventana y contemplar el paisaje, tendremos que traspasar el espejo.
					
				</div>
				<div class="panel-footer">
					CASETTI, Francesco. Teorías del cine. Madrid: Cátedra, 1994 p. 55
				</div>
			</div>
		</div>
		<div class="col-sm-4"> 
			<div class="panel panel-primary">
				<div class="panel-heading">LOS DERECHOS HUMANOS DE LA MUJER BRASILEÑA</div>
				<div class="panel-body">
					En Brasil, las organizaciones de derechos de la mujer abrieron un nuevo espacio para la
					participación de las mujeres en la vida nacional, trabajando en el contexto de los esfuerzos
					iniciados a principios de los años 80 con el fin de reorganizar la sociedad y lograr que el
					ejercicio de la democracia fuera cada vez más eficaz. Como consecuencia de esta apertura,
					se han tomado iniciativas importantes tanto en el sector público como en el privado para
					combatir la discriminación contra la mujer y sus efectos. El movimiento de mujeres en
					Brasil, apoyado por las acciones de cientos de organizaciones no gubernamentales que
					trabajan en el área de los derechos de la mujer, ha estado muy activo cabildeando a favor
					de estos derechos y ha realizado grandes esfuerzos a fin de encontrar medidas concretas
					para la protección al derecho de una vida libre de violencia. A pesar de los avances, todavía
					se observa la persistencia de prejuicios contra la mujer en varias esferas.
					A pesar de que la participación de la mujer en la vida nacional en Brasil há avanzado mucho
					es ampliamente reconocido que ella continúa estando insuficientemente representada en
					las instituciones del Estado y tiene un acceso limitado a los altos cargos del servicio civil y a
					los cargos de elección popular.

				</div>
				<div class="panel-footer">Texto adaptado de:  http://www.cidh.oas.org/countryrep/Brasesp97/<br>capitulo_8.htm</div>
			</div>
		</div>
		<div class="col-sm-4"> 
			<div class="panel panel-primary">
				<div class="panel-heading">ADAPTACIONES RESPIRATORIAS AL BUCEO</div>
				<div class="panel-body">
					Las aves y mamíferos buceadores están sometidos, obviamente, a períodos de hipoxia
					durante la inmersión. El sistema nervioso central de los vertebrados no puede resistir la
					anoxia y debe suministrársele oxígeno durante el buceo. Los animales buceadores han
					resuelto este problema utilizando reservas corporales de oxígeno. Para atenuar el
					agotamiento de los depósitos disponibles, el oxígeno es utilizado solamente por el cerebro y
					el corazón durante el buceo, el flujo de sangre al resto de órganos se reduce y estos tejidos
					adoptan vías metabólicas anaeróbicas. Existe un notable descenso en la frecuencia cardíaca
					y una reducción del gasto cardíaco. La sangre es recirculada a través del cerebro y el
					corazón.

				</div>
				<div class="panel-footer">(ECKERT, R. Et. Al. Intercambio de gases: Fisiologia Animal.
				Madrid, ed. Interamericana. 1989) </div>
			</div>
		</div>
	</div>
</div><br>

<div class="container">    
	<div class="row">
		<div class="col-sm-4">
			<div class="panel panel-primary">
				<div class="panel-heading">PATOS SILVESTRES</div>
				<div class="panel-body">
					Un grupo muy numeroso de aves acuáticas son los patos. En Chile existen veintiún especies
					diferentes, entre ellas, algunos muy pequeños y escasos como el "Pato rinconero", ave que
					tiene la rara costumbre de no construir nido propio, poniendo sus huevos en nidos de otros
					patos, taguas u otras aves.
					Otro pato admirable por sus costumbres es el "Pato cortacorrientes", que vive solo en ríos
					y esteros correntosos. Su costumbre de remontar torrentes y de nadar por rápidos es única
					entre los patos.
					El "Pato rana" prefiere zambullirse para buscar el alimento en el fondo del agua. Es
					excelente nadador y buceador; sin embargo, no vuela bien. Construye nidos toscos de
					totoras. Es frecuente que algunos huevos se caigan al agua debido a que el nido muchas
					veces es confeccionado con poca perfección. Los huevos son grandes, blancos y tienen
					cáscara rugosa. Los patitos nacidos tienen el cuerpo cubierto de pelusa negra. Son
					excelentes nadadores e zambullidores. Desde el primer día de vida bucean por su propio
					alimento. Sólo en primavera lucen los machos adultos plumaje, de llamativos colores. Las
					hembras, crías y machos fuera de la época reproductiva tienen plumas café grisáceas.

				</div>
				<div class="panel-footer">(Extraído de: Aves de Chile. Thomas Daskan & Jürgen Rottmann, Publicaciones Lo Castillo,
				ed. 2, 1986)</div>
			</div>
		</div>
		<div class="col-sm-4"> 
			<div class="panel panel-primary">
				<div class="panel-heading">SALA DE ESPERA</div>
				<div class="panel-body">
					Costa y Wright roban una casa. Costa asesina a Wright y se queda con la valija llena de joyas y
					dinero. Va a la estación para escaparse con el primer tren. En la sala de espera, una señora se
					sienta a su izquierda y le da conversación. Fastidiado, Costa finge con un bostezo que tiene
					sueño y que va a dormir, pero oye que la señora continúa conversando. Abre entonces los ojos
					y ve, sentado a la derecha, el fantasma de Wright. La señora atraviesa a Costa de lado a lado
					con la mirada y charla con el fantasma, quien contesta con simpatía. Cuando llega el tren, Costa
					trata de levantarse, pero no puede. Está paralizado, mudo y observa atónito cómo el fantasma
					toma tranquilamente la valija y camina con la señora hacia el andén, ahora hablando y
					riéndose. Suben, y el tren parte. Costa los sigue con los ojos. Viene un hombre y comienza a
					limpiar la sala de espera, que ahora está completamente desierta. Pasa la aspiradora por el
					asiento donde está Costa, invisible.
				</div>
				<div class="panel-footer">(Imbert, Enrique A. "Sala de Espera" in Esto Funciona/B Madrid: Ed Pragma, 1986)</div>
			</div>
		</div>
		<div class="col-sm-4"> 
			<div class="panel panel-primary">
				<div class="panel-heading">ESPACIOS NATURALES MADRILEÑOS</div>
				<div class="panel-body">La naturaleza madrileña, como la propia comunidad autónoma que la alberga, es, por
					fuerza, territorialmente pequeña, pero densa, múltiple y variada en sus constrastes. Cinco
					millones de habitantes en torno a una de las mayores urbes1
					europeas es un factor
					indudablemente problemático a la hora de enfocar la conservación, declaración y gestión
					de sus diversos espacios naturales. En síntesis, dos son las grandes líneas de problemas
					madrileños. En primer lugar, la polución industrial y urbana sobre sus ríos, afluentes del
					Tajo, un auténtico atentado ecológico crónicamente establecido tanto en el llamado
					Callejón de Henares como en el Jarama medio. En segundo lugar, la explosión urbanística de
					los habitantes de la ciudad en busca de su segunda residencia, que lleva llenando
				anárquicamente, desde hace más de dos décadas el espacio rural madrileño. (...)</div>
				<div class="panel-footer">(El País Semanal, Número 634, Madrid. 1989)</div>
			</div>
		</div>
	</div>
</div>

<script>
	document.getElementById('espanhol').className = 'active';
</script>
@endsection