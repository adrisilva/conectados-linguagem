@extends('layouts.app')

@section('content')
<style>
    #body p{ float: center; padding-right: 30px; text-transform: capitalize; }
</style>


<section class="background-gray-lightest" style="position: relative;top: -20px;">
    <div class="container">
        <div class="breadcrumbs">
            <ul class="breadcrumb">
                <li><a href="{{ url('home') }}">Home</a></li>
                <li>Sobre a Olimpíada</li>
            </ul>
        </div>
        
        @include('layouts.alerts.success')

        <h1>Fique ligado</h1>
        <p class="lead">No final de cada ano, no 4° bimestre a olimpíada do CPLC será iniciada. Este site irá informar as datas de inscrições! A partir disso, você pode inscrever a sua equipe. </p>
    </div>
</section>

<div class="panel-heading">
    <section class="blog-post">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="post-content margin-bottom--big">
                        <p><strong>Ela funciona da seguinte forma:</strong> A olimpíada é feita de acordo com os livros e mídias que o projeto indicará durante o ano letivo.  Ela tem como objetivo testar os conhecimentos adquiridos,por meio das indicações, durante o ano. Para participar dela é necessário montar uma equipe com três integrantes da sua escola
                        e um professor, pois ele é quem deve acompanhar o desenvolvimento do aluno durante o 
                        processo de uso desse sistema. Além disso, sua equipe acumulará pontos e poderá até receber um certificado disponível para impressão</p>
                        <h2>Sobre a equipe</h2>
                        <ol>
                            <li>Escolha bem quem estará na sua equipe</li>
                            <li>Fique atento a tudo que for indicado no projeto.</li>
                        </ol>
                        <blockquote>
                            <p>O aluno participante do projeto deve acompanhar as notícias
                            do CPLG, pois nele está as indicações de livros, músicas, poemas e outros, que serão lidos e analisados.
                            Com isso, o aluno estará se preparando para a olimpíada, que foi criada para insentivar a leitura do aluno, e também para o vestibular e ENEM, alimentando assim o seu intelecto. <br>
                            A equipe é formada por 3 alunos e um professor, forme sua equipe e vamos vencer!</p>
                        </blockquote>
                        <h3>Papel do professor</h3>
                        <p>Neste projeto os alunos são acompanhados pelos professores
                        da áreas de linguagens e códigos das escolas em que atuam.  Eles irão orientar os alunos quanto aos livros que serão
                        indicados pelo projeto. 
                        Na olimpíada, a equipe pode ter um professor orientador, ele pode participar de mais de uma equipe. É na resolução de questões que o professor pode auxiliar os alunos.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    document.getElementById('home').className = "active";
</script>
@endsection
