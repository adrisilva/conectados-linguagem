@extends('layouts.app')
@section('content')

{!! Form::open(['method' => 'POST', 'url' => 'team-store']) !!}

<h3>Forme Sua Equipe</h3>

<div class="row">
		@if ($errors->any())
		<div class="alert alert-danger" role="alert">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif

</div>

<div class="form-group row">	
	{!! Form::label('name', 'Nome da Equipe', ['class' => 'col-sm-2']) !!}
	<div class="col-sm-10">
		{!! Form::text('name', null,['class' => 'form-control', 'placeholder' => 'Nome da Equipe']) !!}
	</div>
</div>

<div class="form-group row">	
	{!! Form::label('user_one', 'Primeiro Integrante', ['class' => 'col-sm-2']) !!}
	<div class="col-sm-10">
		{!! Form::text('user_one', Auth::user()->name, ['class' => 'form-control', 'readonly' => true]) !!}
	</div>
</div>

<div class="form-group row">	
	{!! Form::label('user_two', 'Segundo Integrante', ['class' => 'col-sm-2']) !!}
	<div class="col-sm-10">
		<select name="user_two" id="user_two" class="form-control">
			<option value="">Selecione o segundo integrante da equipe</option>
			@forelse($alunos as $aluno)
			<option value="{!! $aluno->id !!}">{!! $aluno->name !!}</option>
			@empty
			<option value="">Nenhum aluno registrado</option>
			@endforelse
		</select>
	</div>
</div>

<div class="form-group row">	
	{!! Form::label('user_three', 'Terceiro Integrante', ['class' => 'col-sm-2']) !!}
	<div class="col-sm-10">
		<select name="user_three" id="user_three" class="form-control" disabled>
			<option value="">Selecione o terceiro integrante da equipe</option>
		</select>
	</div>
</div>

<div class="form-group row">	
	{!! Form::label('teacher', 'Professor Responsável', ['class' => 'col-sm-2']) !!}
	<div class="col-sm-10">
		<select name="teacher" id="teacher" class="form-control">
			<option value="">Selecione o professor responsável pela equipe</option>
			@forelse($professores as $professor)
			<option value="{!! $professor->id !!}">{!! $professor->name !!}</option>
			@empty
			<option value="">Nenhum professor registrado</option>
			@endforelse
		</select>
	</div>
</div>

<div class="form-group row">
	<div class="col-sm-2"></div>
	<div class="col-sm-10">
		{!! Form::submit('Cadastrar',['class' => 'btn btn-primary btn-block']) !!}
	</div>
</div>

{!! Form::close() !!}

<br>
<script>
	document.getElementById('olimpiada').className = "active";
</script>

@section('post-scripts')
<script>
	$('select[name=user_two]').change(function(){

		let id_aluno = $(this).val();
		let url = '{!! url('get-alunos/id') !!}'.replace('id', id_aluno);

		if(id_aluno != ""){

			$.get(url, function(aluno){
				$('select[name=user_three]').empty();
				$('select[name=user_three]').append('<option value="">Selecione o terceiro integrante da equipe</option>');

				$.each(aluno, function(key, value){
					$('select[name=user_three]').append('<option value='+ value.id +'>'+ value.name +'</option>');
				});
			});

			$('#user_three').prop('disabled', false);

		}else{
			$('#user_three').prop('disabled', true);				
		}

	});
</script>
@endsection

@endsection