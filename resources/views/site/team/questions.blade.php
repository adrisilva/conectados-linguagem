@extends('layouts.app')
@section('content')

<body>
    @if(empty($score))
    <div class="container">
        <form class="form-horizontal" id="formQuestions" action="{{ url('notes') }}" method="post">
        @csrf
           <h1>Você está na Olimpíada do Conectados Pela Leitura</h1>
           <p>Agora você poderá testar seus conhecimentos de formar divertida!</p> 

           <ul class="list-group">
                <li class="list-group-item" style="border-left: 5px solid #ef5285;">  
                    <div class="container mt-3">
                    <div class="media border p-3">
                    <div class="media-body"> 
                    <h4>1. (UNIVEST) <small><i> Triste Fim de Policarpo Quaresma, de Lima Barreto, é</i></small></h4>
                        <p><input type="radio" id="q1" name="q1" value="a"> a narrativa da vida e morte de um funcionário humilde conformado com a realidade social do seu tempo.</p>
                        <p><input type="radio" name="q1" value="b"> uma autobiografia em que a personagem-título expõe sua insatisfação com relação a burocracia carioca.</p>
                        <p><input type="radio" name="q1" value="c"> o relato das aventuras de um nacionalista ingênuo e fanático que lidera um grupo de oposição no início da República.</p>
                        <p><input type="radio" name="q1" value="d"> um livro de memórias em que o personagem-título, através de um artifício narrativo, conta as atribulações de sua vida até a hora da morte.</p>
                        <p><input type="radio" name="q1" value="e"> a história de um nacionalista fanático que, quixotescamente, tenta resolver sozinho os males sociais de seu tempo.</p>       
                    </div>
                    </div>
                    </div>
                </li>

                <li class="list-group-item" style="border-left: 5px solid #ef5285;">
                    <div class="container mt-3">
                    <div class="media border p-3">
                    <div class="media-body">
                    <h4>2. (PUC)  <small><i>Da personagem que dá título ao romance Triste Fim de Policarpo Quaresma, podemos afirmar que</i></small></h4>
                        <p><input type="radio" name="q2" value="a"> foi um nacionalista extremado, mas nunca estudou com afinco as coisas brasileiras.</p>
                        <p><input type="radio" name="q2" value="b"> perpetrou seu suicídio, porque se sentia decepcionado com a realidade brasileira.</p>
                        <p><input type="radio" name="q2" value="c"> defendeu os valores nacionais, brigou por eles a vida toda e foi condenado à morte injustamente por valores que defendia.</p>
                        <p><input type="radio" name="q2" value="d"> foi considerado traidor da pátria, porque participou da conspiração contra Floriano Peixoto.</p>
                        <p><input type="radio" name="q2" value="e"> era um louco e, por isso, não foi levado a sério pelas pessoas que o cercavam.</p>
                    </div>
                    </div>
                    </div>
                </li>

                <li class="list-group-item" style="border-left: 5px solid #ef5285;">
                    <div class="container mt-3">
                    <div class="media border p-3">
                    <div class="media-body">
                    <h4>3. (FUVEST)<small><i>No romance Triste Fim de Policarpo Quaresma, o nacionalismo exaltado e delirante da personagem principal motiva seu engajamento em três diferentes  <br>projetos, que objetivam “reformar” o país. Esses projetos visam, sucessivamente, aos seguintes setores da vida nacional:</i></small></h4>
                        <p><input type="radio" name="q3" value="a"> escolar, agrícola e militar;</p>
                        <p><input type="radio" name="q3" value="b"> lingüístico, industrial, e militar;</p>
                        <p><input type="radio" name="q3" value="c"> cultural, agrícola e político;</p>
                        <p><input type="radio" name="q3" value="d"> lingüístico, político e militar;</p>
                        <p><input type="radio" name="q3" value="e"> cultura, industrial e político.</p>
                    </div>
                    </div>
                    </div>
                </li>

                <li class="list-group-item" style="border-left: 5px solid #ef5285;">
                    <div class="container mt-3">
                    <div class="media border p-3">
                    <div class="media-body">
                    <h4>4. (FUVEST / GV) <small><i>O meu fim evidente era atar as duas pontas da vida, e restaurar na velhice a adolescência. Pois, senhor, não consegui recompor <br> o que foi nem o que fui.
                    É o que diz o narrador no segundo capítulo do romance Dom Casmurro. Afinal por que não teria ele alcançado o seu intento?</i></small></h4>
                        <p><input type="radio" name="q4" value="a"> Pelas dificuldades inerentes à estrutura do romance, na recuperação de outros tempos.</p>
                        <p><input type="radio" name="q4" value="b"> Pelo receio de confessar suas fraquezas e a traição sofrida.</p>
                        <p><input type="radio" name="q4" value="c"> Porque era impossível recuperar o sentido daquele período, pois ele já não era a mesma pessoa.</p>
                        <p><input type="radio" name="q4" value="d"> Pela falta de bom senso e de clareza na apreensão das lembranças.</p>
                        <p><input type="radio" name="q4" value="e"> Porque o tempo, impiedoso, apaga todos os acontecimentos e transforma as pessoas.</p>
                    </div>
                    </div>
                    </div>    
                </li>

                <li class="list-group-item" style="border-left: 5px solid #ef5285;">
                    <div class="container mt-3">
                    <div class="media border p-3">
                    <div class="media-body">
                    <h4>5. (UFL) <small><i> Todas as alternativas apresentam informações sobre Dom Casmurro, de Machado de Assis, EXCETO: </i></small></h4>
                        <p><input type="radio" name="q5" value="a"> A questão do adultério, tratada de forma ambígua pelo autor, permanece em aberto no fim da narrativa.</p>
                        <p><input type="radio" name="q5" value="b"> O narrador, através do exercício da memória, busca ligar o presente ao passado, a velhice à adolescência.</p>
                        <p><input type="radio" name="q5" value="c"> O narrador protagonista, ao assumir a primeira pessoa, apresenta uma visão tendenciosa dos acontecimentos.</p>
                        <p><input type="radio" name="q5" value="d"> O autor, introduzindo-se na narrativa, fornece ao leitor informações que contradizem as opiniões do narrador.</p>
                        <p><input type="radio" name="q5" value="e"> A narrativa, marcada pela ironia, mantém uma relação intersexual com a tragédia Otelo, de Shakespeare.</p>
                    </div>
                    </div>
                    </div>
                </li>

                <li class="list-group-item" style="border-left: 5px solid #ef5285;">
                    <div class="container mt-3">
                    <div class="media border p-3">
                    <div class="media-body">
                    <h4>6. (FUVEST / GV) <small><i> "O meu fim evidente era atar as duas pontas da vida, e restaurar na velhice a adolescência. Pois, senhor, não consegui recompor o que <br> foi nem o que fui."
                    É o que diz o narrador no segundo capítulo do romance Dom Casmurro. Afinal por que não teria ele alcançado o seu intento? </i></small></h4>
                        <p><input type="radio" name="q6" value="a"> Pelas dificuldades inerentes à estrutura do romance, na recuperação de outros tempos.</p>
                        <p><input type="radio" name="q6" value="b"> Pelo receio de confessar suas fraquezas e a traição sofrida.</p>
                        <p><input type="radio" name="q6" value="c"> Porque era impossível recuperar o sentido daquele período, pois ele já não era a mesma pessoa.</p>
                        <p><input type="radio" name="q6" value="d"> Pela falta de bom senso e de clareza na apreensão das lembranças.</p>
                        <p><input type="radio" name="q6" value="e"> Porque o tempo, impiedoso, apaga todos os acontecimentos e transforma as pessoas.</p> 
                    </div>
                    </div>
                    </div>
                </li>

                <li class="list-group-item" style="border-left: 5px solid #ef5285;">
                    <div class="container mt-3">
                    <div class="media border p-3">
                    <div class="media-body">
                    <h4>7. (UFPR)  <small><i> A propósito de Dom Casmurro, de Machado de Assis, é correto afirmar: </i></small></h4>
                        <p><input type="radio" name="q7" value="a"> A narrativa de Bento Santiago é comparável a uma acusação: aproveitando sua formação jurídica, o narrador pretende configurar a culpa de Capitu.</p>
                        <p><input type="radio" name="q7" value="b"> O artifício narrativo usado é a forma de diário, de modo que o leitor receba as informações do narrador à medida que elas acontecem, mantendo-se assim a tensão.</p>
                        <p><input type="radio" name="q7" value="c"> Elegendo a temática do adultério, o autor resgata o romantismo de seus primeiros romances, com personagens idealizadas entregues à paixão amorosa.</p>
                        <p><input type="radio" name="q7" value="d"> O espaço geográfico e social representado é situado em uma província do Império, buscando demonstrar que as mazelas sociais não são prerrogativa da Corte.</p>
                        <p><input type="radio" name="q7" value="e"> Bentinho desejava a morte de Escobar (até tentou envenená-lo uma vez), a ponto de se sentir culpado quando o ex-amigo morreu afogado.</p>
                    </div>
                    </div>
                    </div>
                </li>

                <li class="list-group-item" style="border-left: 5px solid #ef5285;">
                    <div class="container mt-3">
                    <div class="media border p-3">
                    <div class="media-body">
                    <h4>8.  (Conc. Federal)  <small><i> Machado de Assis, em Dom Casmurro, mostra Capitu como personagem de maior destaque. Bentinho nutria, em relação a ela, sentimentos de </i></small></h4>
                        <p><input type="radio" name="q8" value="a"> profunda admiração e respeito.</p>
                        <p><input type="radio" name="q8" value="a"> amor desmedido.</p>
                        <p><input type="radio" name="q8" value="a"> verdadeira veneração de ordem espiritual.</p>
                        <p><input type="radio" name="q8" value="a"> ciúme exacerbado, raiando os limites de doença mental.</p>
                        <p><input type="radio" name="q8" value="a"> nenhuma admiração ou respeito.</p>
                    </div>
                    </div>
                    </div>
                </li>

                <li class="list-group-item" style="border-left: 5px solid #ef5285;">
                    <div class="container mt-3">
                    <div class="media border p-3">    
                    <div class="media-body">
                    <h4>9. (FUVEST) <small><i> Podemos afirmar que na obra D. Casmurro, Machado de Assis:</i></small></h4>
                        <p><input type="radio" name="q9" value="a"> defende a tese de que o meio determina o homem porque descreve a personagem Capitu desde o início como uma futura adúltera.</p>
                        <p><input type="radio" name="q9" value="b"> defende a tese determinista porque o meio em que Bentinho e Capitu vivem determina a futura tragédia.</p>
                        <p><input type="radio" name="q9" value="c"> não defende a tese determinista, apontando antagonismo entre o meio e a tragédia final.</p>
                        <p><input type="radio" name="q9" value="d"> defende a tese determinista ao demonstrar a influência da educação religiosa na formação de Capitu.</p>
                        <p><input type="radio" name="q9" value="e"> não defende a tese determinista de modo explícito porque não fica clara a relação entre o meio e o fim trágico dos personagens.</p>
                    </div>
                    </div>
                    </div>
                </li>

                <li class="list-group-item" style="border-left: 5px solid #ef5285;">
                    <div class="container mt-3">
                    <div class="media border p-3">
                    <div class="media-body">
                    <h4>10. (MACK-SP) <small><i> Assinale a alternativa onde aparece uma característica que não se aplica à obra de Augusto dos Anjos.</i></small></h4>
                        <p><input type="radio" name="q10" value="a"> referência à decomposição da matéria.</p>
                        <p><input type="radio" name="q10" value="b"> pessimismo diante da vida.</p>
                        <p><input type="radio" name="q10" value="c"> amor reduzido a instinto.</p>
                        <p><input type="radio" name="q10" value="d"> incorporação de vocabulário científico.</p>
                        <p><input type="radio" name="q10" value="e"> nacionalismo exaltado.</p>
                    </div>
                    </div>
                    </div>
                </li>
            </ul>
            <div class="form-group">        
                <div class="col-sm-offset-2 col-sm-10" style="width: 100%;position: relative; left: -200px;">
                    <input type="submit" class="btn btn-default btn-block btn-lg" name="button" value="Enviar" style="width: 100%;">
                </div>
            </div>
        </form>
    </div>
    @else
        <div class="container">
            <h3>Você já respondeu as perguntas, e sua pontuação foi: {!! $score->points !!}</h3>
        </div>
    @endif

    @section('post-scripts')
        <script>
            $('#formQuestions').validate({
                rules: {
                    q1: {
                        required: true
                    },
                    q2: {
                        required: true
                    },
                    q3: {
                        required: true
                    },
                    q4: {
                        required: true
                    },
                    q5: {
                        required: true
                    },
                    q6: {
                        required: true
                    },
                    q7: {
                        required: true
                    },
                    q8: {
                        required: true
                    },
                    q9: {
                        required: true
                    },
                    q10: {
                        required: true
                    }
                },
                errorPlacement: function(error, element){
                    
                },

            });

            
        </script>
    @endsection
</body>


@endsection