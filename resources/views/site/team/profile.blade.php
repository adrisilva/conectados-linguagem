@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-sm-4">
		<div class="post">
			<div class="image"><a href="#"><img src="{!! asset('images/perfil.gif') !!}" alt="" class="img-responsive"></a></div>
			<h3>
				<a href="#">Primeiro Integrante 
					<small>
						<i style="font-size: 17px;">
							{!! $team->student_one->name !!}, aluno
						</i>
					</small>
				</a>
			</h3>
			<p class="post__intro">
				Idade: {!! \App\Util\Util::calculateAge($team->student_one->birth) !!} anos <br>
				Cursando: {!! $team->student_one->series !!}º ano do ensino médio <br>
				@if($team->student_one->course != null)
				Curso: {!! $team->student_one->course !!}
				@endif
			</p>
		</div>
	</div>

	<div class="col-sm-4">
		<div class="post">
			<div class="image"><a href="#"><img src="{!! asset('images/perfil.gif') !!}" alt="" class="img-responsive"></a></div>
			<h3>
				<a href="#">Segundo Integrante 
					<small>
						<i style="font-size: 17px;">
							{!! $team->student_two->name !!}, aluno
						</i>
					</small>
				</a>
			</h3>
			<p class="post__intro">
				Idade: {!! \App\Util\Util::calculateAge($team->student_two->birth) !!} anos <br>
				Cursando: {!! $team->student_two->series !!}º ano do ensino médio <br>
				@if($team->student_one->course != null)
				Curso: {!! $team->student_one->course !!}
				@endif
			</p>

		</div>
	</div>

	<div class="col-sm-4">
		<div class="post">
			<div class="image"><a href="#"><img src="{!! asset('images/perfil.gif') !!}" alt="" class="img-responsive"></a></div>
			<h3>
				<a href="#">Terceiro Integrante 
					<small>
						<i style="font-size: 17px;">
							{!! $team->student_three->name !!}, aluno
						</i>
					</small>
				</a>
			</h3>
			<p class="post__intro">
				Idade: {!! \App\Util\Util::calculateAge($team->student_three->birth) !!} anos <br>
				Cursando: {!! $team->student_three->series !!}º ano do ensino médio <br>
				@if($team->student_one->course != null)
				Curso: {!! $team->student_one->course !!}
				@endif
			</p>

		</div>
	</div>

	<div class="col-sm-4">
		<div class="post">
			<div class="image"><a href="#"><img src="{!! asset('images/prof.gif') !!}" alt="" class="img-responsive"></a></div>
			<h3>
				<a href="#">Professor Responsável
					<small>
						<i style="font-size: 17px;">
							{!! $team->teacher_responsible->name !!}, professor
						</i>
					</small>
				</a>
			</h3>
			<p class="post__intro">
				Matéria: {!! $team->teacher_responsible->matter !!}
			</p>
		</div>
	</div>

	<div class="col-sm-4">
		<div class="post">
			<div class="image"><a href="#"><img src="{!! asset('images/ponto.gif') !!}" alt="" class="img-responsive"></a></div>
			<h3><a href="#">Pontuação <small><i> Nunca desista!</i></a></h3>
				<p class="post__intro"> 
					Quantidade de pontos: 
					@if($score != 0)
						{!! $score !!}
					@else
						Sem Pontuação
					@endif
				</p>

			</div>
		</div>

	</div>
</div>


</div>

@endsection