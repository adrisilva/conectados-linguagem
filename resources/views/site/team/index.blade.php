@extends('layouts.app')
@section('content')

<h3>Equipes</h3>

<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<td>Nome Equipe</td>
			<td>Data de Criação</td>
			<td>Ver Integrantes</td>
			<td>Remover</td>
		</tr>
	</thead>
	<tbody>
		@forelse($teams as $team)
			<tr>
				<td>{!! $team->name !!}</td>
				<td>{!! Carbon\Carbon::parse($team->created_at)->format('d/m/Y') !!}</td>
				<td><a href="{!! route('team.show', [$school, $team->id]) !!}" class="btn btn-primary btn-sm"><i class="fas fa-eye"></i></a></td>
				<td><a href="{!! route('team.delete', [$school, $team->id]) !!}" class="btn btn-primary btn-sm"><i class="fas fa-close"></i></a></td>
			</tr>
		@empty
			<tr><td colspan="4">Você não está responsável por nenhuma equipe</td></tr>
		@endforelse
	</tbody>
</table>

@endsection
