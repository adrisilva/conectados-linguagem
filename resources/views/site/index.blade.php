@extends('layouts.app')

@section('content')

<div style="background: url(images/f2.jpg); position: relative; top: -44px;">
  <div class="container">
   
    
    <div class="main">
      <div class="buttons">
        <div id="prev" class="disc-button">&lt;</div>
        <div id="next" class="disc-button">&gt;</div>
      </div>
      <div id="hexaflip-demo4" class="demo"></div>
      
    </div>
  </div>
  <script>
    var hexaDemo4,
	    images = [
	    './images/logo.png',
	    './images/tress.jpg',
	    './images/dois.jpg',
	    './images/cinco.jpg',
	    './images/um.jpg',
	    './images/seis.jpg'
    ];
    
    document.addEventListener('DOMContentLoaded', function(){
      hexaDemo4 = new HexaFlip(document.getElementById('hexaflip-demo4'), {set: images},{
        size: 500
      });
      document.getElementById('prev').addEventListener('click', function(){
        hexaDemo4.flipBack();
      }, false);
      
      document.getElementById('next').addEventListener('click', function(){
        hexaDemo4.flip();
      }, false);

    }, false);

  </script>
</div>

<section class="background-gray-lightest negative-margin">
	<div class="container">
		<h1> Quem somos</h1>
		<p class="lead">O Conectados Pela Leitura é um projeto que visa o incentivo da leitura dos jovens do Estado do Ceará, visto que existe um desinteresse constante pela leitura na juventude atual. Esse projeto irá trazer à juventude a oportunidade de construir uma melhor criticidade e, além disso, poderá alimentar seu intelecto nos três anos do ensino médio.  Dessa forma, ele estará preparado para um futuro vestibular e conseguirá incluir-se no âmbito profissional.</p>
		<p> <a href="text.html" class="btn btn-ghost">Continue reading   </a></p>
	</div>
</section>

<div class="container-fluid bg-grey">
	<div class="row">
		<div class="col-sm-4">
			<span class="glyphicon glyphicon-globe logo" style="color: #ef5285;"></span>
		</div>
		<div class="col-sm-8">
			<h2>Nosso valores</h2>
			<h4><strong>MISSÃO:</strong> Nossa missão é ajudar a juventude na interpretação de texto e faze-la se apaixonar pela leitura.</h4>      
			<p><strong>VISÃO:</strong> Nossa visão é atingir o máximo de indivíduos possível.</p>
		</div>
	</div>
</div>

<div class="container-fluid text-center">
	<h2>SERVIÇOS</h2>
	<h4>O que nós oferecemos</h4>
	<br>
	<div class="row">
		<div class="col-sm-4">
			<span class="glyphicon glyphicon-off logo-small" style="color: #ef5285;"></span>
			<h4>PODER</h4>
			<p>Você poderá adquirir o poder do conhecimento.</p>
		</div>
		<div class="col-sm-4">
			<span class="glyphicon glyphicon-heart logo-small" style="color: #ef5285;"></span>
			<h4>AMOR</h4>
			<p>Você poderá se apaixonar pela leitura.</p>
		</div>
		<div class="col-sm-4">
			<span class="glyphicon glyphicon-lock logo-small" style="color: #ef5285;"></span>
			<h4>TAREFA CONCLUÍDA</h4>
			<p>Você pode aprender de diferentes formas.</p>
		</div>
	</div>
	<br><br>
	<div class="row">
		<div class="col-sm-4">
			<span class="glyphicon glyphicon-leaf logo-small" style="color: #ef5285;"></span>
			<h4>Verde</h4>
			<p>O projeto é online para não utilizar meios orgânicos como a folha.</p>
		</div>
		<div class="col-sm-4">
			<span class="glyphicon glyphicon-certificate logo-small" style="color: #ef5285;"></span>
			<h4>CERTIFICAÇÃO</h4>
			<p>Você pode receber um certificado na olimpíada. </p>
		</div>
		<div class="col-sm-4">
			<span class="glyphicon glyphicon-wrench logo-small" style="color: #ef5285;"></span>
			<h4 style="color:#303030;">TRABALHO DURO</h4>
			<p>É necessário esforço para atingir um objetivo.</p>
		</div>
	</div>
</div>

<script>
	document.getElementById('home').className = "active";
</script>

@endsection