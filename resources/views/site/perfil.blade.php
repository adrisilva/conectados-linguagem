@extends('layouts.app')
@section('content')

<!DOCTYPE html>
<html>
<head>

	<title></title>
</head>
<body>
  <div class="container">
   <p><h1>Meu Perfil</h1></p>
   <div class="form-group">
     <p><h3 style="color: #ef5285">Informações Pessoais</h3></p>
     <table class="table table-bordered">
      <thead>
        <tr>
          <th>Nome</th>
          <th>Idade</th>
          <th>Escola</th>
          @if($user->type == 'Aluno')
            <th>Série</th>
          @else
            <th>Matéria</th>
          @endif
          <th>Cidade</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td> {!! $user->name !!} </td>
          <td> {!! \App\Util\Util::calculateAge($user->birth) !!} </td>
          <td> EEEP Professor Onélio Porto </td>
          @if($user->type == 'Aluno')
            <td>{!! $user->series !!}</td>
          @else
            <td>{!! $user->matter !!}</td>
          @endif
          <td> Fortaleza</td>
        </tr>
      </tbody>
    </table>
    <br><br>
    @if($team_name != null)
      <p><h3 style="color: #ef5285">Sua Equipe</h3></p>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Nome da Equipe</th>
            <th>Integrante 1</th>
            <th>Integrante 2</th>
            <th>Integrante 3</th>
            <th>Professor</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td> {!! $team_name !!} </td>
            <td> {!! $team->student_one->name !!} </td>
            <td> {!! $team->student_two->name !!} </td>
            <td> {!! $team->student_three->name !!} </td>
            <td> {!! $team->teacher_responsible->name !!} </td>
          </tr>
        </tbody>
      </table>
    @endif
  </div>
</div>

</body>
</html>

@endsection()