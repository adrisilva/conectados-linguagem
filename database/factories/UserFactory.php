<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/


$factory->define(App\Models\User::class, function (Faker $faker) {


	$course = ['Informática', 'Enfermagem', 'Finanças', 'Produção de Moda'];
	$series = [1, 2, 3];
	$number_random = rand(0, 9);
	$reg = $number_random;

	for($i = 1; $i <= 6; $i++){
		$number_random = rand(0, 9);
		$reg .= $number_random;
	}

    return [
        'name' 				=> $faker->name,
        'email' 			=> $faker->unique()->safeEmail,
        'password' 			=> bcrypt(123456), 
        'registration'		=> $reg,
        'birth'				=> $faker->date($format = 'Y-m-d', $max = 'now'),
        'course'			=> array_random($course),
        'series'        	=> array_random($series),
        'shift'         	=> 'Integral',
        'type'          	=> 'Aluno',
        'remember_token' 	=> str_random(10),
    ];
});
