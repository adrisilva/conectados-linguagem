<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table){
            $table->unsignedInteger('school_id')->nullable();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');
        });

        Schema::table('teams', function(Blueprint $table){
            $table->unsignedInteger('school_id')->nullable();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');
        });

        Schema::table('admins', function(Blueprint $table){
            $table->unsignedInteger('school_id')->nullable();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');
        });

        Schema::table('scores', function(Blueprint $table){
            $table->unsignedInteger('school_id')->nullable();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');
        });

        Schema::table('system_configs', function(Blueprint $table){
            $table->unsignedInteger('school_id')->nullable();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table){
            $table->dropForeign('school_id');
            $table->dropColumn('school_id');
        });

        Schema::table('teams', function(Blueprint $table){
            $table->dropForeign('school_id');
            $table->dropColumn('school_id');
        });

        Schema::table('admins', function(Blueprint $table){
            $table->dropForeign('school_id');
            $table->dropColumn('school_id');
        });

        Schema::table('scores', function(Blueprint $table){
            $table->dropForeign('school_id');
            $table->dropColumn('school_id');
        });

        Schema::table('system_configs', function(Blueprint $table){
            $table->dropForeign('school_id');
            $table->dropColumn('school_id');
        });
    }
}
