<?php

use Illuminate\Database\Seeder;
use App\Models\Admin;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
        	'name' 			=> 'Administrador',
        	'email'         => 'admin@gmail.com',
            'phone'         => '40028922',
        	'password'      => bcrypt('admin')
        ]);

    }
}
