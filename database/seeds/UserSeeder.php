<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'name' 			=> 'José Silva',
        	'registration'	=> '123456',
        	'birth'			=> '2000/05/07',
        	'course'		=> 'Informática',
        	'series'        => '3',
        	'shift'         => 'Integral',
        	'type'          => 'Aluno',
        	'email'         => 'jose@gmail.com',
        	'password'      => bcrypt(123456),
        ]);

        User::create([
        	'name' 			=> 'Maria Silva',
        	'registration'	=> '654321',
        	'birth'			=> '2000/05/08',
        	'course'		=> 'Informática',
        	'series'        => '3',
        	'shift'         => 'Integral',
        	'type'          => 'Aluno',
        	'email'         => 'maria@gmail.com',
        	'password'      => bcrypt(123456),
        ]);

        User::create([
        	'name' 			=> 'Paulo Silva',
        	'registration'	=> '012345',
        	'birth'			=> '2000/05/09',
        	'course'		=> 'Informática',
        	'series'        => '3',
        	'shift'         => 'Integral',
        	'type'          => 'Aluno',
        	'email'         => 'paulo@gmail.com',
        	'password'      => bcrypt(123456),
        ]);

        User::create([
            'name'          => 'Paula Silva',
            'registration'  => '765123',
            'birth'         => '1985/06/07',
            'type'          => 'Professor',
            'matter'        => 'Gramática',
            'email'         => 'paula@gmail.com',
            'password'      => bcrypt(123456),
        ]);
    }
}
