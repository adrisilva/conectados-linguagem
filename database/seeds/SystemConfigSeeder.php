<?php

use App\Models\SystemConfig;
use Illuminate\Database\Seeder;


class SystemConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SystemConfig::create([
        	'name' 	=> 'issues_released',
            'label' => 'Liberar questões da olímpiada'
        ]);
    }
}
