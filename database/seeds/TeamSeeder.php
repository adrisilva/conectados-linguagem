<?php

use Illuminate\Database\Seeder;
use App\Models\Team;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Team::create([
        	'name'	   	 => 'Millenium',
        	'user_one' 	 => 1,
        	'user_two' 	 => 2,
        	'user_three' => 3,
        	'teacher'	 => 4
        ]);
    }
}
