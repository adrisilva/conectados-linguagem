<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if(env('APP_ENV') == 'production'){
            $this->call(AdminSeeder::class);
            $this->call(SystemConfigSeeder::class);
        }else{
            $this->call(UserSeeder::class);
            $this->call(TeamSeeder::class);
            $this->call(AdminSeeder::class);
             $this->call(SystemConfigSeeder::class);
            factory(App\Models\User::class, 30)->create();
        }
    }
}
